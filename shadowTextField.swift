//
//  shadowView.swift
//  lendnow.ai
//
//  Created by WELCOME on 05/12/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//
import Foundation
import UIKit

class shadowTextField: UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 0
        layer.shadowOffset =  CGSize(width: 0, height: 1)   // CGSizeMake(0, 1)
        layer.shadowColor = UIColor.black.cgColor
       // layer.shadowRadius = 2
        layer.shadowOpacity = 0.3
       
    }
    
}

