//
//  UserRegistrationController.swift
//  lendnow.ai
//
//  Created by Nishant Kumar on 15/09/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import UIKit
import RealmSwift

class UserRegistrationController: UIViewController {
    
    @IBOutlet weak var mobileNumber: UITextField!
    
    let appSettings = UserDefaults.standard
    let userPhoneNumber = "userPhoneNumber"
    
    var registerationService :RegistrationService!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Find the path below")
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
   
    @IBAction func registerNumber(_ sender: Any) {
        
//        UserDefaults.standard.set(mobileNumber.text, forKey: "loginmobilenum")
        
        
        
        let usersParams = ["notify":true, "phoneCountryCode": "+1", "phoneNumber":self.mobileNumber.text] as [String : Any]
        
        let parameters = ["monthlyIncome": 0,"outstandingBalanceDue": 0, "requestAvailed": 0, "requestsInQueue": 0, "tcvBalance": 0, "user": usersParams] as [String : Any]
        print(parameters)
        guard let url = URL(string: "http://lendnowai.centralus.cloudapp.azure.com:8080/lendnowai_core/admin/users/borrowers") else { return }
        print(url)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print("response below \n")
                print(response)
            }
            if let data = data {
                do {
                    print("release do")
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(" release json below \n")
                    print(json)
                    print("json id...----->")
                    
                    if let json = json as? [String: Any] {
                        // now you have a top-level json dictionary
                        for (key,value) in json {
                            if key == "id" {
                                print("key value is id")
                                if value != nil {
                                    print("value is not nil")
                                    
                                    DispatchQueue.main.async {
                                        self.appSettings.set(self.mobileNumber.text, forKey: self.userPhoneNumber)
                                        UserDefaults.standard.set("profile", forKey: "initialtab")
                                        self.gotoOTPController()
                                    }
                                    
                                }
                            } else {
//                                self.mobileNumber.text = ""
                                /** let alert = UIAlertController(title: "LendNow.AI", message: "Mobile number already registered", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)**/
                            }
                        }
                    }
                    
                } catch {
                    print("release error below \n")
                    print(error)
                   
                }
            }
            }.resume()
        
        
        
        
        
      /**  var disableMyButton = sender as? UIButton
        disableMyButton?.isEnabled = false
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.startAnimating()
        view.addSubview(myActivityIndicator)
        
        
       registerationService = RegistrationService()
        registerationService.registerUserNumber(userPhoneNumber: self.mobileNumber.text) { (user) in
            myActivityIndicator.stopAnimating();
            disableMyButton?.isEnabled=true
            if let user = user {
                
                DispatchQueue.main.async {
                    // print("\(user.phoneNumber)")
                   
                    self.performSegue(withIdentifier: "signUpToOtp", sender: user)
                    
                }
                let realm = try! Realm()
                // Persist your data easily
                try! realm.write {
                    realm.add(user)
                }
                
            } else {
                print("error crtin----------------------")
                self.mobileNumber.text = ""
                let alert = UIAlertController(title: "bozucu", message: "Account already exists/ Invalid number", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)

            }
        } **/

    }
    func gotoOTPController() {
        let otpController = storyboard?.instantiateViewController(withIdentifier: "OtpController") as! OtpController
        present(otpController, animated: true, completion: nil)
    }
   /** override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("its being called");
        guard segue.identifier == "signUpToOtp" else {
            return
        }
        guard let otpController = segue.destination as? OtpController
            else{
                return
        }
        guard let user = sender as AnyObject as? User else{
            return
        }
        /** print("User details " )
        print(user.id)
        otpController.user=user **/
    }**/
    
}

