//
//  shadowModel.swift
//  lendnow.ai
//
//  Created by Urmila on 10/14/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import Foundation
import UIKit

class shadowModel: UILabel
{
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.shadowOffset =  CGSize(width: 0, height: 1)   // CGSizeMake(0, 1)
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 1.5
        layer.shadowOpacity = 0.65
        layer.cornerRadius = 0
    }
}
