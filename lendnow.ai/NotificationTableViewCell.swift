//
//  NotificationTableViewCell.swift
//  lendnow.ai
//
//  Created by Sundar Gsv on 11/11/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var purchaseDateLabel: UILabel!
   
    @IBOutlet weak var merchantNameLabel: UILabel!
    
    @IBOutlet weak var newLowerUnitPriceLabel: UILabel!

    
    func setCell(item: Notifications)
    {
        purchaseDateLabel.text = "\(item.purchaseDate)"
        merchantNameLabel.text = "\(item.merchantName)"
        newLowerUnitPriceLabel.text = "\(item.newLowerUnitPrice)"

    }

}
