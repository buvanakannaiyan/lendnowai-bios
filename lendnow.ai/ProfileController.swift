//
//  ProfileController.swift
//  bozucu
//
//  Created by Selvakumar on 19/09/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import UIKit

class ProfileController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "main-bg.jpg")
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)        // Do any additional setup after loading the view, typically from a nib.
    }

    
}
