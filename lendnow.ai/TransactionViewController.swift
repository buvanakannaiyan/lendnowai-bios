//
//  TransactionViewController.swift
//  lendnow.ai
//
//  Created by Urmila on 10/10/17.
//  Modified by Sundar Gsv on 26/10/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import UIKit
import RealmSwift
import Cloudinary
import SystemConfiguration

class TransactionViewController: UIViewController, UIImagePickerControllerDelegate, UIPopoverControllerDelegate, UINavigationControllerDelegate {
    
    // MARK : ProductImageUpload helper params
    
    let picker: UIImagePickerController? = UIImagePickerController()
    let cloudinary = CLDCloudinary(configuration: CLDConfiguration(cloudinaryUrl: "cloudinary://341447472638319:OAkljZsMuPUhdPZ8TZ54OdWM-ok@dzp9fse75")!)
    var chosenImage: UIImage?
    var savedProductName = ""
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var tableviewCell: UITableView!
    
  
    
    @IBOutlet weak var viewForShadow: UIView!
    var phoneNumber : String=""
    var transactions  = [Purchase]()

    
    var purchaseService :PurchaseService!
    let appSettings = UserDefaults.standard
    var refreshControl: UIRefreshControl = UIRefreshControl()
  
    // Mark : Cell Expantion/ collapsing
    var sPath: IndexPath?
    var selectedCellIndexPath: IndexPath?
    public var selectedCellHeight: CGFloat = 310.0
    public var unselectedCellHeight: CGFloat = 78.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(TransactionViewController.functionToCall), name: NSNotification.Name(rawValue: "notificationName"), object: nil)
        picker?.delegate = self

        
        
         //self.messageLabel.isHidden = true
        let internetStatus: Bool = isInternetAvailable()
        if (internetStatus)
        {
            print("Yesss....Net Connected...!")
            self.tableviewCell.tableFooterView = UIView(frame: CGRect.zero)
            self.tableviewCell.delegate = self
            self.tableviewCell.dataSource = self
            self.tableviewCell.showsVerticalScrollIndicator = false
            
            
            if let userPhoneNumber = UserDefaults.standard.value(forKey: "userPhoneNumber"){
                self.phoneNumber = userPhoneNumber as! String
            }
            
            self.fetchAllFromDisc()
            
            tableviewCell.backgroundColor = UIColor(red: 243.0/255, green: 243.0/255, blue: 243.0/255, alpha: 1.0)
            
            self.navigationController?.isNavigationBarHidden = true
            
            refreshControl.tintColor = UIColor.blue
            refreshControl.backgroundColor = UIColor(red: 243.0/255, green: 243.0/255, blue: 243.0/255, alpha: 1.0)
            refreshControl.attributedTitle = NSAttributedString(string: "Loading...", attributes: [NSForegroundColorAttributeName: refreshControl.tintColor])
            refreshControl.addTarget(self, action: #selector(TransactionViewController.reload), for: UIControlEvents.valueChanged)
            
            if #available(iOS 10.0, *){
                tableviewCell.refreshControl = refreshControl
            }else {
                tableviewCell.addSubview(refreshControl)
            }

        }else {
            print("Not connected...")
            self.messageLabel.isHidden = false
            self.messageLabel.text = "You are in offline mode. Please connect your internet."
        }
        
       
    }
    
    // MARK : ProductImageUpload delegate func
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("picker cancel.")
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        self.chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        dismiss(animated: true, completion: nil)
        self.uploadAction()
    }
    
    
    func uploadAction(){
        let params = CLDUploadRequestParams()
        params.setFolder("product_images")
        
        let uploader = self.cloudinary.createUploader()
        uploader.upload(data: UIImageJPEGRepresentation(self.chosenImage!, 0.8)!, uploadPreset: "hvceethh", params: params){ result, error in
            
            do {
                let publicId = try result?.publicId
                let url = result?.url

                // START - Update the productImage URL with core-bozucu-backend
                var params: [String : Any] =
                    [   "productName": "\(self.savedProductName)",
                        "imageUrl": url
                ]
                print(params)
                let jsonData = try? JSONSerialization.data(withJSONObject: params)
                let updateImageURL = URL(string: "http://consumerai.centralus.cloudapp.azure.com:8080/consumerai_core/productRecord/updateImage")!
                var request = URLRequest(url: updateImageURL)
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.httpMethod = "POST"
                request.httpBody = jsonData
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200{
                        // no exception & reload the controller
                        DispatchQueue.main.async {
                            // Update UI
                            self.viewDidLoad()
                            self.viewWillAppear(true)
                        }

                        print("Success")

                    }else {
                        let responseString = String(data: data, encoding: String.Encoding.utf8)
                        // Exception
                        print("responseString = \(responseString)")
                    }
                }
                
                task.resume()
                // END - Update the productImage URL with core-bozucu-backend
                
            } catch {
                print(error)
               
            }
            
        }
   
    }
    
    func takePicture(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker?.allowsEditing = false
            picker?.sourceType = UIImagePickerControllerSourceType.camera
            picker?.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
        
    }
    
    func openGallary()
    {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }

    
    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    
    func fetchAllFromDisc() {
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        myActivityIndicator.color=UIColor.blue
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.startAnimating()
        view.addSubview(myActivityIndicator)
        
        purchaseService = PurchaseService()
        purchaseService.getStaticPurchase(userPhoneNumber: self.phoneNumber, searchQuery: "", pageIndex: 1  ){ (transactionsFromAPI) in
            self.transactions = []
            if transactionsFromAPI != nil  {
                DispatchQueue.main.async {
                    for trans in transactionsFromAPI {
                        self.transactions.append(trans)
                        self.messageLabel.isHidden = true
                    }
                    myActivityIndicator.stopAnimating()
                    self.view.endEditing(true)
                    self.tableviewCell.reloadData()
                    
                }
                myActivityIndicator.stopAnimating()
            }
        }
    }
    
    // Mark : Refresh to sync/ cache from first pageIndex = 0
    func reload() {
        print("reloading")
        // START - [HIT API TO RELOAD]
        purchaseService = PurchaseService()
        purchaseService.getStaticPurchase(userPhoneNumber: self.phoneNumber, searchQuery: "", pageIndex: 1  ){ (transactionsFromAPI) in
            self.transactions = []
            if transactionsFromAPI != nil  {
                DispatchQueue.main.async {
                    self.messageLabel.isHidden = true
                    for trans in transactionsFromAPI {
                        self.transactions.append(trans)
                    }
                    self.tableviewCell.reloadData()
                    self.refreshControl.endRefreshing()

                }
            }
        }
        // END - [HIT API TO RELOAD]
    }
    
    // Mark : Cell Expansion/ collapsing
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if selectedCellIndexPath == indexPath{
            
            let transaction = transactions[indexPath.row]
            
                return selectedCellHeight
            
        }
        return unselectedCellHeight
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedCellIndexPath != nil && selectedCellIndexPath == indexPath {
            selectedCellIndexPath = nil
        } else {
            selectedCellIndexPath = indexPath
        }
        
        if selectedCellIndexPath != nil {
            // This ensures, that the cell is fully visible once expanded
            tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated: true)
            // tableView.scrollToRowat: atAtIndexPath(indexPath, atScrollPosition: .none, animated: true)
        }

        sPath=indexPath;
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText \(searchText)")
        
    }
    
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        
    }
    
    func functionToCall(notification: NSNotification) {
        //This function will be called when you post the notification
         let options = notification.userInfo!["selection"] as! NSString
         let productName = notification.userInfo!["productName"] as! NSString
         self.savedProductName = productName as String
        if ( options == "photos") {
            self.openGallary()
        }
        else{
            self.takePicture()
        }
    }
    
    
}

extension TransactionViewController: UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate{
    
    func tableView(_ tableviewcustcell: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactions.count
    }
    
    func tableView(_ tableviewcustcell: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let transaction = transactions[indexPath.row]
        let cell = tableviewCell.dequeueReusableCell(withIdentifier: "TransCustomId") as! TransCustomTableViewCell
        
        cell.contentView.backgroundColor = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
    
        cell.selectionStyle = .none
        
        cell.clipsToBounds = true
        
        cell.setCell(item: transaction)
        
        
        return cell
    }
    
}

