//
//  GeneralService.swift
//  lendnow.ai
//
//  Created by Sundar Gsv on 17/09/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//
import UIKit
import Alamofire


class GeneralService {
    var window: UIWindow?
    var userService : UserService!
    
    let consumerAIBaseURL: URL?
    
    init()
    {
        consumerAIBaseURL = URL(string: "http://consumerai.centralus.cloudapp.azure.com:8080/consumerai_core/")
    }

    
    func actionDelete() {
        
    }
    
    func updateFCM(userPhoneNumber: Any,token : Any, completion: @escaping (Any?) -> Void)
    {
        print("inside updateFCM")
        let params: [String : Any] =
            [
                "token": token,
                "phoneNumber": userPhoneNumber
                
                
        ]
        
        // POST request
        let FCMBaseURL: String = "fcm/updateToken"
        if let FCMURL = URL(string: "\(consumerAIBaseURL)\(FCMBaseURL)") {
            
            
            Alamofire.request(FCMURL, method: .post, parameters: params,encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                print("after api call")
                if let jsonDictionary = response.result.value as? [String : Any] {

                    let message = jsonDictionary["message"] as? String
                    print(message ?? "Empty msg")
                    completion(message)
                }
                else {
                    completion(nil)
                }
            })
        }
        
    }

}
