//
//  Purchase.swift
//  lendnow.ai
//
//  Created by Sundar Gsv on 19/09/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import Foundation
import ObjectMapper

class PurchaseRecords:  NSObject, Mappable
{
    dynamic var pageIndex: Int = 0
    dynamic var pageSize: Int = 0
    dynamic var totalRecords: Int = 0
    dynamic var totalPages: Int = 0
   // var results = List<Purchase>()
    
    
    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    /*
     * The below struct is used to represent the model without any problem using Alamofire
     * Eventhough the keys were changed from Back-end. Just change the struct accordingly which
     * prevents changing the all over the code
     */
    
    func mapping(map: Map) {
        
        pageIndex <- map["pageIndex"]
        pageSize <- map["pageSize"]
        totalRecords <- map["totalRecords"]
        totalPages <- map["totalPages"]
        
        var results: [Purchase]?

    }

}
