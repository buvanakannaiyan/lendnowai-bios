//
//  AuthViewController.swift
//  lendnow.ai
//
//  Created by Sundar Gsv on 15/09/17.
//  Copyright © 2017 Magnum opus, Inc. All rights reserved.
//

import UIKit
import LocalAuthentication

class AuthViewController: UIViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        authenticateUsingTouchId()
    }
    
    func authenticateUsingTouchId() {
        let authContext: LAContext = LAContext()
        let authReason = "Authenticate via Touch ID"
        var authError: NSError?
        
        if authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            authContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: authReason, reply: { (success, error) in
                if success {
                    print("Authenticate successfully")
                    
                    
                    //want to deal with UI code here
                    DispatchQueue.main.async {
                         print("auth done")
                        self.performSegue(withIdentifier: "authToTabBarID", sender: nil)
                    }
                    
                } else {
                    if let error = error {
                        // Go to main Queue and report the error
                            // can now execute UI code here
                        DispatchQueue.main.async {
                            self.reportTouchIDError(error: error as NSError)
                        }
                    
                    }
                }
            })
        } else {
            //We have an error
            print(authError?.localizedDescription ?? "Not Authenticated")
            
            //show the normal sign up screen
            
            
        }
        
    }
    
    func reportTouchIDError(error: NSError) {
        switch error.code {
        case LAError.authenticationFailed.rawValue:
            print("Authentication failed")
        case LAError.passcodeNotSet.rawValue:
            print("Passcode not set")
        case LAError.systemCancel.rawValue:
            print("Authentication was canceled by the system")
        case LAError.userCancel.rawValue:
            print("User cancel auth")
        case LAError.touchIDNotEnrolled.rawValue:
            print("User hasn't enrolled any finger with touch id")
        case LAError.touchIDNotAvailable.rawValue:
            print("Touch ID not available")
        case LAError.userFallback.rawValue:
            print("User tapped enter password")
        default:
            print(error.localizedDescription)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "authToTabBarID" else {
            return
        }
        guard let tabBarController = segue.destination as? tabBarViewController
            else{
                return
        }
        
    }
    
    
}
