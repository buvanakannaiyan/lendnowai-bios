//
//  profileLabelShadow.swift
//  bozucu
//
//  Created by MAC on 10/14/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import Foundation
class shadow{
    
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    }
    
    
    func addShadow(shadowColor: CGColor = UIColor.blackColor().CGColor,
                   shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
}
}
