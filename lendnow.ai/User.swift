//
//  User.swift
//  lendnow.ai
//
//  Created by Sundar Gsv on 15/09/17.
//  Copyright © 2017 Magnum opus, Inc. All rights reserved.
//

// JSON: javascript object notation

import Foundation
import ObjectMapper
import RealmSwift

class User:  Object, Mappable
{
    dynamic var id: String = ""
    dynamic var phoneCountryCode: String = ""
    dynamic var phoneNumber: String = ""
    dynamic var firstName: String = ""
    dynamic var lastName: String = ""
    dynamic var notify = false
    dynamic var authorizedToRepresent = false
    dynamic var disbursement: String = ""
    dynamic var userEmail: String = ""
    dynamic var uploadEmail: String = ""
    dynamic var imageUrl: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    /*
     * The below struct is used to represent the model without any problem using Alamofire
     * Eventhough the keys were changed from Back-end. Just change the struct accordingly which
     * prevents changing the all over the code
     */
    
    func mapping(map: Map) {
        id <- map["id"]
        phoneCountryCode <- map["phoneCountryCode"]
        phoneNumber <- map["phoneNumber"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        notify <- map["notify"]
        authorizedToRepresent <- map["authorizedToRepresent"]
        disbursement <- map["disbursement"]
        userEmail <- map["userEmail"]
        uploadEmail <- map["uploadEmail"]
        imageUrl <- map["imageUrl"]
    }
}























