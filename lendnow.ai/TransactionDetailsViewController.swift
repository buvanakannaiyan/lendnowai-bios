//
//  TransactionDetailsViewController.swift
//  lendnow.ai
//
//  Created by Sundar Gsv on 11/11/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import UIKit

class TransactionDetailsViewController: UIViewController {

    @IBOutlet weak var notificationTableView: UITableView!
    var phoneNumber : String = ""
    var notifications  = [Notifications]()
    
    
    var purchaseService :PurchaseService!
    let appSettings = UserDefaults.standard
    var refreshControl: UIRefreshControl = UIRefreshControl()
    
    // Mark : Cell Expantion/ collapsing
    var sPath: IndexPath?
    var selectedCellIndexPath: IndexPath?
    let selectedCellHeight: CGFloat = 348.0
    let unselectedCellHeight: CGFloat = 70.0
    
    // MARK : Transaction >> ProductName
    var name: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print("\(name)")
        self.notificationTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.notificationTableView.delegate = self
        self.notificationTableView.dataSource = self
        self.notificationTableView.showsVerticalScrollIndicator = false
        
        
        if let userPhoneNumber = UserDefaults.standard.value(forKey: "userPhoneNumber"){
            self.phoneNumber = userPhoneNumber as! String
        }
        
        self.fetchAllFromDisc()
        
        notificationTableView.backgroundColor = UIColor(red: 243.0/255, green: 243.0/255, blue: 243.0/255, alpha: 1.0)
        
        self.navigationController?.isNavigationBarHidden = true
        
        refreshControl.tintColor = UIColor.blue
        refreshControl.backgroundColor = UIColor(red: 243.0/255, green: 243.0/255, blue: 243.0/255, alpha: 1.0)
        refreshControl.attributedTitle = NSAttributedString(string: "Loading...", attributes: [NSForegroundColorAttributeName: refreshControl.tintColor])
        refreshControl.addTarget(self, action: #selector(TransactionViewController.reload), for: UIControlEvents.valueChanged)
        
        if #available(iOS 10.0, *){
            notificationTableView.refreshControl = refreshControl
        }else {
            notificationTableView.addSubview(refreshControl)
        }

    }

    func fetchAllFromDisc() {
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        myActivityIndicator.color=UIColor.blue
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.startAnimating()
        view.addSubview(myActivityIndicator)
        
        purchaseService = PurchaseService()
        purchaseService.getNotificationByProductName(userPhoneNumber: phoneNumber, productName: name) { (notificationsFromAPI) in
            self.notifications = []
            if notificationsFromAPI != nil  {
                DispatchQueue.main.async {
                    for notify in notificationsFromAPI {
                        self.notifications.append(notify)
                    }
                    myActivityIndicator.stopAnimating()
                    self.view.endEditing(true)
                    self.notificationTableView.reloadData()
                    
                }
                myActivityIndicator.stopAnimating()
            }

            
        }
    
    
    }
    
    // Mark : Cell Expansion/ collapsing
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if selectedCellIndexPath == indexPath{
            return selectedCellHeight
        }
        return unselectedCellHeight
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedCellIndexPath != nil && selectedCellIndexPath == indexPath {
            selectedCellIndexPath = nil
        } else {
            selectedCellIndexPath = indexPath
        }
        
        if selectedCellIndexPath != nil {
            // This ensures, that the cell is fully visible once expanded
            tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated: true)
            // tableView.scrollToRowat: atAtIndexPath(indexPath, atScrollPosition: .none, animated: true)
        }

        sPath=indexPath;
        tableView.beginUpdates()
        tableView.endUpdates()
    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    @IBAction func handleBack(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)

        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "transactionDetailToTab") as! tabBarViewController
        nextViewController.navigateFrom = "TransactionDetails"

        self.present(nextViewController, animated: true, completion: nil)
    }
}


extension TransactionDetailsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ notificationTableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ notificationTableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let notification = notifications[indexPath.row]
        let cell = notificationTableView.dequeueReusableCell(withIdentifier: "NotificationsCustomId") as! NotificationTableViewCell
        
        cell.contentView.backgroundColor = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        cell.selectionStyle = .none
        
        cell.clipsToBounds = true
        
        cell.setCell(item: notification)
        
        
        return cell
    }
    
}
