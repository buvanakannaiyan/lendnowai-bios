//
//  PurchaseViewController.swift
//  lendnow.ai
//
//  Created by Sundar Gsv on 18/09/17.
//  Copyright © 2017 magnum opus. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift
let cellID = "cell"

class PurchaseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate ,UITextFieldDelegate{
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var purchaseTableView: UITableView!
    var refreshControl: UIRefreshControl = UIRefreshControl()
    
    var arrRes = [Purchase]()
    var arrayLoaded: Bool = false
    var data = [
        ["123 iPhone6",       "500 MacBook2000",      "1000 windows10",    "400 watch",
         "200 iPad",     "567 Macbook",    "234 Valleyball",    "245 Badminton"],
        ["2000 Apple",        "180 Pear",      "350 Strawberry",    "1000 Avocado",
         "345 Banana",       "3000 Grape",     "345 Melon",         "300 Orange",
         "466 Peach",        "355 Kiwi"],
        ["2000 Apple",        "180 Pear",      "350 Strawberry",    "1000 Avocado",
         "345 Banana",       "3000 Grape",     "345 Melon",         "300 Orange",
         "466 Peach",        "355 Kiwi"],
        ["2000 Apple",        "180 Pear",      "350 Strawberry",    "1000 Avocado",
         "345 Banana",       "3000 Grape",     "345 Melon",         "300 Orange",
         "466 Peach",        "355 Kiwi"]
        
    ]
    
    var selectedIndexPath : NSIndexPath?
    
    var p: Int!
    var purchaseService :PurchaseService!
    var phoneNumber : String=""
    var selectedIndex : NSInteger! = -1
    var selectedCellIndexPath: IndexPath?
    let selectedCellHeight: CGFloat = 227.0
    let unselectedCellHeight: CGFloat = 42.0
    
    // Mark : the below is for purchase caching
    let realm = try! Realm()
    let appSettings = UserDefaults.standard
    var allPurchaseNextPageNo = 0
    var allPurchaseNextPageNoKey = "allPurchaseNextPageNoKey"
    var allPurchaseCachedTotalRecords = 0
    var allPurchaseCachedTotalRecordsKey = "allPurchaseCachedTotalRecordsKey"
    
    override func viewDidLoad() {

        searchBar.delegate=self
        // self.navigationController?.isNavigationBarHidden = true
        
        if let userPhoneNumber = UserDefaults.standard.value(forKey: "userPhoneNumber"){
            self.phoneNumber = userPhoneNumber as! String
        }
        // Register the cell here
        let nib = UINib(nibName: "purchaseCell", bundle: nil)
        purchaseTableView.register(nib, forCellReuseIdentifier: "purchaseCell")
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PurchaseViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        tap.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(tap)
        p = 0
        // self.updatePurchaseDetails(noOfDays: "30",searchQuery: "")
        self.getCachedAllPurchase(noOfDays: "30",searchQuery: "")
        
        refreshControl.tintColor = UIColor.blue
        refreshControl.backgroundColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: "Syncing...", attributes: [NSForegroundColorAttributeName: refreshControl.tintColor])
        refreshControl.addTarget(self, action: #selector(PurchaseViewController.refreshData), for: UIControlEvents.valueChanged)
        
        if #available(iOS 10.0, *){
            purchaseTableView.refreshControl = refreshControl
        }else {
            purchaseTableView.addSubview(refreshControl)
        }
        
    }
    
    //
    // @param : nil
    // @description : To call the getPurchase service and reload the arrRes
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        
        self.view.endEditing(true)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true) //This will hide the keyboard
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    var searchActive : Bool = false
    var searchValue : String=""
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func updatePurchaseDetails(noOfDays : String,searchQuery : String) -> Bool
    {
        
        
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        myActivityIndicator.color=UIColor.blue
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.startAnimating()
        view.addSubview(myActivityIndicator)
        
        purchaseService = PurchaseService()
        purchaseService.getPurchaseByDays(userPhoneNumber:phoneNumber,noOfDays: noOfDays,searchQuery: searchQuery ){ (notifications) in
            self.arrRes=[]
            if notifications != nil  {
                DispatchQueue.main.async {
                    for notify in notifications {
                        self.arrRes.append(notify)
                    }
                    myActivityIndicator.stopAnimating()
                    self.view.endEditing(true)
                    self.purchaseTableView.reloadData()
                    
                }
                myActivityIndicator.stopAnimating()
            }
        }
        
        return true
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.view.endEditing(true)
        var modifiedSearchValue : String = searchValue.replacingOccurrences(of: " ", with: "%20")
        // self.updatePurchaseDetails(noOfDays: "", searchQuery: modifiedSearchValue)
        self.getCachedAllPurchase(noOfDays: "", searchQuery: modifiedSearchValue)
        
        
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchValue=searchText
        if(searchText.isEmpty)
        {
            self.view.endEditing(true)
            
            if(p == 0)
            {
                self.getCachedAllPurchase(noOfDays: "30", searchQuery: "")
                
            }
            else{
                self.getCachedAllPurchase(noOfDays: "", searchQuery: "")
                
            }
            
        }
        
    }
    @IBAction func search(_ sender: Any) {
        let alert = UIAlertController(title: "Search Purchase", message: "enter product name", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.text = ""
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            print("Text field: \(textField?.text)")
            let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
            myActivityIndicator.color=UIColor.blue
            //myActivityIndicator.center = view.center
            myActivityIndicator.hidesWhenStopped = true
            myActivityIndicator.startAnimating()
            alert?.view.addSubview(myActivityIndicator)
            myActivityIndicator.isUserInteractionEnabled = false
            
            Alamofire.request("http://api.bozucu.ai/consumerai_core/purchaseRecords?userPhoneNumber="+self.phoneNumber+"&productNameFilter=" + (textField?.text)!).responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    
                    if let resData = swiftyJsonVar.arrayObject {
                        // print(self.arrRes)
                    }
                    if self.arrRes.count > 0 {
                        self.purchaseTableView.reloadData()
                    }
                    myActivityIndicator.stopAnimating()
                }
            }
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    // Mark : Refresh to sync/ cache from first pageIndex = 0
    func refreshData() {
        var result: Bool = false
        if p == 1{
            if result == self.updateCache(noOfDays: ""){
                self.getCachedAllPurchase(noOfDays: "", searchQuery: "")
                if(self.arrayLoaded) {
                    self.purchaseTableView.reloadData()
                    refreshControl.endRefreshing()
                }
            }else{
                print("Nothing to update")
                refreshControl.endRefreshing()
            }
            
            
        }
        else{
            if result == self.updateCache(noOfDays: "30"){
                self.getCachedAllPurchase(noOfDays: "30", searchQuery: "")
                if(self.arrayLoaded) {
                    self.purchaseTableView.reloadData()
                    refreshControl.endRefreshing()
                }
            }else{
                print("Nothing to update")
                refreshControl.endRefreshing()
            }
            
        }
        
        self.purchaseTableView.reloadData()
        
        
        
    }
    
    func updateCache(noOfDays : String) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRes.count
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = purchaseTableView.dequeueReusableCell(withIdentifier: "purchaseCell") as! purchaseCell
        let product = arrRes[indexPath.row]
        cell.customInit(daysRemain: (product.daysRemain) , savedAmount: (product.savedAmount), purcahseAmount: (product.purcahseAmount), productName: (product.productName), storeName: (product.storeName), notificationCount: product.notificationCount )
        
        cell.contentView.backgroundColor = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        let whiteRoundedView : UIView = UIView(frame: CGRect(x: 6, y: 6, width: 330, height: 215))
        
        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [243.0, 243.0, 243.0, 1.0])
        whiteRoundedView.layer.masksToBounds = false
        whiteRoundedView.layer.cornerRadius = 2.0
        whiteRoundedView.layer.shadowOffset = CGSize(width: -1, height: 1)
        whiteRoundedView.layer.shadowOpacity = 0.2
        
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubview(toBack: whiteRoundedView)
        
        // cell.backgroundColor = UIColor.white
        cell.selectionStyle = .none

        cell.clipsToBounds = true
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if selectedCellIndexPath == indexPath{
            return selectedCellHeight
        }
        return unselectedCellHeight
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedCellIndexPath != nil && selectedCellIndexPath == indexPath {
            selectedCellIndexPath = nil
        } else {
            selectedCellIndexPath = indexPath
        }
        
        
        tableView.beginUpdates()
        tableView.endUpdates()
        
        if selectedCellIndexPath != nil {
            // This ensures, that the cell is fully visible once expanded
            tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated: true)
            // tableView.scrollToRowat: atAtIndexPath(indexPath, atScrollPosition: .none, animated: true)
        }
        
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func switchPurchaseTableView(_ sender: UISegmentedControl) {
        p = (sender as AnyObject).selectedSegmentIndex
        
        
        if p == 1{
            // self.updatePurchaseDetails(noOfDays: "", searchQuery: "")
            self.getCachedAllPurchase(noOfDays: "", searchQuery: "")
            
        }
        else{
            // self.updatePurchaseDetails(noOfDays: "30", searchQuery: "")
            self.getCachedAllPurchase(noOfDays: "30", searchQuery: "")
            
        }
        purchaseTableView.reloadData()
    }
    
    
    // Mark:
    func getCachedAllPurchase(noOfDays : String, searchQuery : String){
 
    }
    
    
    @IBAction func loadMore(_ sender: UIButton) {
    
    }
    
    
}
