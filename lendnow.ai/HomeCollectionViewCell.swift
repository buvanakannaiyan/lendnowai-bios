
//
//  InterestCollectionViewCell.swift
//  lendnow.ai
//
//  Created by Nishant on 3/6/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var featuredImageView: UIImageView!
//    @IBOutlet weak var interestTitleLabel: UILabel!
//    @IBOutlet weak var backgroundColorView: UIView!
    
    var chart: Chart? {
        didSet {
            self.updateUI()
        }
    }
    
    private func updateUI()
    {
        if let chart = chart {
            featuredImageView.image = chart.featuredImage
        } else {
            featuredImageView.image = nil
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = 3.0
        layer.shadowRadius = 10
        layer.shadowOpacity = 0.4
        layer.shadowOffset = CGSize(width: 5, height: 10)
        
        self.clipsToBounds = false
    }
}


