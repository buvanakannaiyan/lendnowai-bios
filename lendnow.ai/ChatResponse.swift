//
//  ChatResponse.swift
//  lendnow.ai
//
//  Created by Urmila on 10/26/17.
//  Copyright © 2017 MAC. All rights reserved.
//

import Foundation
import ObjectMapper


class ChatResponse: NSObject, Mappable {
    
    var sessionId: String!
    var contexts: String?
    var text: String!
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        sessionId <- map["sessionId"]
        contexts <- map["contexts"]
        text <- map["text"]
        
    }
    
}
