//
//  shadowStackview.swift
//  lendnow.ai
//
//  Created by WELCOME on 09/12/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import UIKit

class shadowStackview: UIStackView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.borderWidth = 5
        layer.borderColor = UIColor.red.cgColor 
        layer.shadowOffset =  CGSize(width: 0, height: 1)   // CGSizeMake(0, 1)
        layer.shadowColor = UIColor.red.cgColor
        layer.shadowRadius = 5
        layer.shadowOpacity = 1.5
        
    }

}
