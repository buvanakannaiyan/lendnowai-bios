//
//  Chart.swift
//  lendnow.ai
//
//  Created by Nishant on 10/10/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import UIKit

class Chart
{
    // MARK: - Public API
    var title = ""
    var featuredImage: UIImage
    
    init(title: String, featuredImage: UIImage)
    {
        self.title = title
        self.featuredImage = featuredImage
    }
    
    // MARK: - Private
    // dummy data
    static func fetchCharts() -> [Chart]
    {
        return [
            Chart(title: "Chart A", featuredImage: UIImage(named: "date")!),
            Chart(title: "Chat B", featuredImage: UIImage(named: "product")!),
            Chart(title: "Chart C", featuredImage: UIImage(named: "store")!),
           
        ]
    }
}
