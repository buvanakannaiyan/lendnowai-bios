//
//  ChatRequest.swift
//  lendnow.ai
//
//  Created by Urmila on 10/25/17.
//  Copyright © 2017 MAC. All rights reserved.
//

import Foundation
import ObjectMapper


class ChatRequest: NSObject, Mappable {
    
    var contexts: String?
    var text: String!
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        contexts <- map["contexts"]
        text <- map["text"]
       
    }
    
}
