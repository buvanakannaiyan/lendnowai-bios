//
//  ProfileViewController.swift
//  lendnow.ai
//
//  Created by Sundar Gsv on 15/09/17.
//  Copyright © 2017 Magnum opus, Inc. All rights reserved.
//


import UIKit
import RealmSwift
import Cloudinary
import SystemConfiguration

class ProfileViewController: UIViewController ,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate {
    
    let picker: UIImagePickerController? = UIImagePickerController()
    let cloudinary = CLDCloudinary(configuration: CLDConfiguration(cloudinaryUrl: "cloudinary://341447472638319:OAkljZsMuPUhdPZ8TZ54OdWM-ok@dzp9fse75")!)
    
    //  @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var ProfileIMG: UIImageView!
    @IBOutlet weak var profileName: UITextField!
    @IBOutlet weak var uploadEmail: UILabel!
    @IBOutlet weak var notificationImage: UILabel!
    @IBOutlet weak var contactNumber: UILabel!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var authoriseReport: UILabel!
    @IBOutlet weak var notification: UILabel!
    
    var userObject: User = User()
    
   
    // @IBOutlet weak var userEmail: shadowTextField!
    
    var usernameTemp: String = ""
    var useremailTemp: String = ""
    
    var phoneNumber: String = ""
    var chosenImage: UIImage?
    var userService: UserService!
    var notificationEnabled: Bool = false
    let realm = try! Realm()
   
       override func viewDidLoad() {
        super.viewDidLoad()

        
        self.cloudinary.cachePolicy = CLDImageCachePolicy.disk
        self.cloudinary.cacheMaxDiskCapacity = 50 * 1024 * 1024

       profileName.delegate = self
        userEmail.delegate = self
        let user = self.realm.objects(User.self)
        picker?.delegate=self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        ProfileIMG.isUserInteractionEnabled = true
        ProfileIMG.addGestureRecognizer(tapGestureRecognizer)
        
        //GUI THINGS
        
        self.navigationController?.isNavigationBarHidden = true
       
        let notificationTap = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.handleNotificationBell))
        self.notification.isUserInteractionEnabled = true
        self.notification.addGestureRecognizer(notificationTap)
        
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        myActivityIndicator.color=UIColor.blue
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.startAnimating()
        view.addSubview(myActivityIndicator)

      do {
        try
        print("in try...")
            //Mark : Get Data's from local storage
        usernameTemp = user[0].firstName
        useremailTemp = user[0].userEmail
          //
            if(user[0].firstName != ""){print("no name from api")
                self.profileName.text = " " + user[0].firstName
            }
            self.contactNumber.text = user[0].phoneNumber
            self.userEmail.text = user[0].userEmail
            self.uploadEmail.text = "lendnow@lendnow.ai"
//            self.uploadEmail.text = user[0].uploadEmail
        print(user[0].imageUrl)
       
        
        if(user[0].imageUrl != ""){
            
                let myImage = cloudinary.createDownloader().fetchImage(user[0].imageUrl, { (totalBytesExpected) in
                    // Handle progress
                    //add smallActivityIndicators
                    print("progressing...")
                }) { (responseImage, error) in
                    if(responseImage != nil){
                        self.ProfileIMG.image = responseImage
                        
                    }else{
                        // set default image
                         self.ProfileIMG.image = UIImage(named: "profile-1.png")
                    }
                    // responseImage is an instance of UIImage
                    // error is an instance of NSError
                }
                
            }else{
                print("imageExists is true")
                 self.ProfileIMG.image = UIImage(named: "profile-1.png")
            }
       
            print(user[0].notify)
            if(!user[0].notify)
            {
                self.notification.text = self.notificationImage.text
                self.notification.textColor = UIColor.black
            }else {
                self.notificationEnabled = true
            }
                myActivityIndicator.stopAnimating()
        
            // Update userObject.phoneNumber used for payload to update the profile on demand
            self.userObject.phoneNumber = user[0].phoneNumber
            self.userObject.firstName = user[0].firstName
            self.userObject.userEmail = user[0].userEmail
        
      }
      catch let error as NSError {
         print(error)
                userService = UserService()
                userService.getProfile(userPhoneNumber: phoneNumber) { (user) in
                        if let user = user {
                            DispatchQueue.main.async {
                                let phoneNumber = user.phoneNumber
                                self.userEmail.text = user.userEmail
                                self.contactNumber.text = phoneNumber
                                self.uploadEmail.text = "lendnow@lendnow.ai"
//                                self.uploadEmail.text = user.uploadEmail
                                if(!user.notify)
                                {
                                    self.notification.text = self.notificationImage.text
                                    self.notification.textColor = UIColor.black
                                }else {
                                    self.notificationEnabled = true
                                }
        
                            }
                        }
                }
        }
 }
    
    func dismissKeyboard() {
        
        if(profileName.text == usernameTemp)
        {
            print("No change in username....")
        }
        else{
            self.userObject.firstName = profileName.text!
            self.patchProfile(userObject: self.userObject)
            print("name changed..........")
            
        }
        if(userEmail.text == useremailTemp)
        {
            print("No change in useremail....")
        }
        else{
            
            self.userObject.userEmail = userEmail.text!
            self.patchProfile(userObject: self.userObject)
            print("email changed..........")
        }
        
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        profileName.resignFirstResponder()
        userEmail.resignFirstResponder()
        if(profileName.text == usernameTemp)
        {
            print("No change in username....")
        }
        else{
            self.userObject.firstName = profileName.text!
            self.patchProfile(userObject: self.userObject)
            print("name changed")
            
        }
        if(userEmail.text == useremailTemp)
        {
            print("No change in useremail....")
        }
        else{
            
            self.userObject.userEmail = userEmail.text!
            self.patchProfile(userObject: self.userObject)
            print("email changed")
        }
        return true
    }
    
    func uploadAction(){
        let params = CLDUploadRequestParams()
        params.setFolder("profile_images")
        
        let uploader = self.cloudinary.createUploader()
        uploader.upload(data: UIImageJPEGRepresentation(self.chosenImage!, 0.8)!, uploadPreset: "hvceethh", params: params){ result, error in
            
            do {
                let publicId = try result?.publicId
                let url = result?.url
                
                // START - Update the productImage URL with core-bozucu-backend
                var params: [String : Any] =
                    [   "userName": "\(self.userObject.firstName)",
                        "imageUrl": url
                ]
                print(params)
                let jsonData = try? JSONSerialization.data(withJSONObject: params)
                let updateImageURL = URL(string: "http://consumerai.centralus.cloudapp.azure.com:8080/consumerai_core/productRecord/updateImage")!
                var request = URLRequest(url: updateImageURL)
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.httpMethod = "POST"
                request.httpBody = jsonData
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200{
                        // no exception & reload the controller
                        DispatchQueue.main.async {
                            // Update UI
                            self.viewDidLoad()
                            self.viewWillAppear(true)
                        }
                        
                        print("Success")
                    }else {
                        let responseString = String(data: data, encoding: String.Encoding.utf8)
                        // Exception
                        print("responseString = \(responseString)")
                    }
                }
                
                task.resume()
                // END - Update the productImage URL with core-bozucu-backend
                
            } catch {
                print(error)
                
            }
            
        }
       
    }
    func takePicture(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker?.allowsEditing = false
            picker?.sourceType = UIImagePickerControllerSourceType.camera
            picker?.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
        
    }
    
    func openGallary()
    {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker!, animated: true, completion: nil)

    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        print("finishUploading")
        self.chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        ProfileIMG.contentMode = .scaleAspectFit
        ProfileIMG.image = chosenImage
        
        dismiss(animated: true, completion: nil)
        self.uploadAction()
        
    }

        func editInfo(_ sender: UIButton)
        {
        var fontDetails:String
        var interactDeatail:Bool
        var titlecolor:UIColor
        if(sender.tag == 1){
            
            fontDetails = ""
            interactDeatail  = false
            sender.tag = 0
            titlecolor = UIColor.white
        }else{
            
            fontDetails = ""
            interactDeatail = true
            sender.tag = 1
            titlecolor = UIColor.green
        }
        sender.setTitle(fontDetails,for: .normal)
        sender.titleLabel?.textColor  = UIColor.red
        
        self.profileName
            .isUserInteractionEnabled = interactDeatail
     //   self.userEmail.isUserInteractionEnabled = interactDeatail
    }
    
    
    // Mark: Handling notificationBell icon on tap - UpdateNotification change

    func handleNotificationBell(sender:UITapGestureRecognizer){
        
        if(self.notificationEnabled){
            self.notification.textColor = UIColor.black
            
        }else {
            self.notification.textColor = UIColor(red: 0.0/255, green: 255.0/255, blue: 0.0/255, alpha: 1.0)
        }
    
        // Update userObject.notify used for payload to update the profile on demand
        self.userObject.notify = !self.notificationEnabled
        self.patchProfile(userObject: self.userObject)
        self.notificationEnabled = !self.notificationEnabled
        if (self.notificationEnabled){
            self.notification.textColor = UIColor(red: 0.0/255, green: 255.0/255, blue: 0.0/255, alpha: 1.0)
        }else{
            self.notification.textColor = UIColor.black
        }
    }
    
    func patchProfile(userObject: User){
        var result = false
        // Update userObject.notify used for payload to update the profile on demand
        // Hit API to update and re-cache the profile data again
        userService = UserService()
        userService.updateUserProfile(user: userObject) { (user) in
            if user != nil {
                DispatchQueue.main.async {
                    
                    try! self.realm.write {
                        self.realm.add(user!, update: true)
                    }
                }
                
            }
           
        }

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("picker cancel.")
        dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func shareOrCopy(_ sender: UIButton) {
  
        let emailId = uploadEmail.text
        print(emailId)
        let textToShare = ["Dear Sir/Madam, I'm using the bozucu app to track my purchase record. Hence, I would like you to send the receipt of my purchase to this email address \(emailId as! String). Appreciate your assistance. Your sincerely, \(contactNumber.text as! String)"]
        
         print(textToShare)
        
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        self.present(activityViewController, animated: true, completion: nil)
        
    }
}

