//
//  LandingPageViewController.swift
//  lendnow.ai
//
//  Created by Sundar Gsv on 16/09/17.
//  Copyright © 2017 Magnum opus, Inc. All rights reserved.
//

import UIKit

class LandingPageViewController: UIViewController {

    @IBOutlet weak var videoView: UIWebView!
   
    @IBOutlet weak var howItWOrks: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        howItWOrks.isEnabled = false
        let youtube = "https://www.youtube.com/embed/ypouJfj_pgg"
        //let url = cloudinary.createUrl().setTransformation(CLDTransformation().setWidth(90).setHeight(130).setGravity(.Face).setCrop(.Fill)).setType(.Facebook).generate("http://res.cloudinary.com/dzp9fse75/video/upload/c_fit/v1514273684/others/bozucu_V1_withaudio_-_converted_with_Clipchamp.mp4")

//        <html><head><title>.</title><style>body,html,iframe{margin:0;padding:0;}</style></head><body><iframe width=\"100%%\"  height=\"250\" src=\"http://www.youtube.com/embed/OsSZ4u7Sz6s\" frameborder=\"0\" allowfullscreen></iframe></body></html>
        videoView.contentMode = UIViewContentMode.center
        videoView.allowsInlineMediaPlayback = true
        
       //let width: CGFloat = videoView.frame.size.width
        let height = "100%" // Assuming that the videos aspect ratio is 16:9
        
        //let htmlString = "<div style=text-align: center;><script type=text/javascript src=http://www.youtube.com/iframe_api></script><script type=text/javascript>function onYouTubeIframeAPIReady(){ytplayer=new YT.Player(playerId,{events:{onReady:onPlayerReady}})}function onPlayerReady(a){a.target.playVideo();}</script><iframe id=playerId type=text/html width=\(width) height=\(height) src=http://www.youtube.com/embed/\(youtube)?enablejsapi=1&rel=0&playsinline=1&autoplay=1 frameborder=0></div>"

      // videoView.loadHTMLString(htmlString, baseURL: nil)
       let wid = "100%"
        videoView.loadHTMLString("<html><head><title>.</title><style>body,html,iframe{margin:0;padding:0;}</style></head><body><iframe align=\"middle\" width=\"\(wid)\" height=\"\(height)\" src=\(youtube)?&playsinline=1\" frameborder=\"0\" allowfullscreen></iframe></body></html>",baseURL:nil)
        
        print("load")
        if let userPhoneNumber = UserDefaults.standard.value(forKey: "userPhoneNumber"){
            print(userPhoneNumber)
            self.performSegue(withIdentifier: "landingPageToAuthID", sender: nil)
        } else {
            print("register now")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "landingPageToAuthID" else {
            return
        }
        guard let authController = segue.destination as? AuthViewController
            else{
                return
        }
    }

}
