//
//  purchaseTableCell.swift
//  lendnow.ai
//
//  Created by Sundar Gsv on 18/09/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import UIKit

class purchaseCell: UITableViewCell {

    
    // W : 320, H : 190
    class var expandedHeight : CGFloat { get { return 215.0 } }
    class var defaultHeight : CGFloat { get { return 100.0 } }
    
    @IBOutlet weak var productImage: UIImageView!
    
    
    @IBOutlet weak var productNameLabel: UILabel!
    
    
    @IBOutlet weak var productUnitPriceLabel: UILabel!
  
    @IBOutlet weak var productPurchaseDateLabel: UILabel!
    
    
    @IBOutlet weak var merchantNameLabel: UILabel!
    
    
    @IBOutlet weak var storeAddressLabel: UILabel!
    
    
    @IBOutlet weak var supressNotificationSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layoutMargins = UIEdgeInsetsMake(8, 0, 8, 0)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func customInit(daysRemain: Int, savedAmount: Double, purcahseAmount: Double, productName: String, storeName: String, notificationCount: Int ) {
        // self.productImage.image = image
        self.productNameLabel.text = productName
        self.productUnitPriceLabel.text = "\(purcahseAmount)"
        // self.expiringDays.text = "\(daysRemain) Days to go"
        // self.productPurchaseDateLabel.text = date
        self.merchantNameLabel.text = storeName
        
        self.productNameLabel.textColor = UIColor.black
        // self.productUnitPriceLabel.textColor = UIColor.black
    }
}
