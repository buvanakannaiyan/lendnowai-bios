//
//  ChatService.swift
//  lendnow.ai
//
//  Created by Urmila on 10/26/17.
//  Copyright © 2017 MAC. All rights reserved.
//

import Foundation
import Alamofire

class ChatService
{
    // API url: http://livebot.centralus.cloudapp.azure.com:8080/livebot
    let liveBotURL: URL?
    
    init()
    {
        self.liveBotURL = URL(string: "http://lendnowai.centralus.cloudapp.azure.com:5001/lendnow")
    }
    
    func getResponse(contexts: String, text: String, completion: @escaping (ChatResponse?) -> Void)
        
    {
        print("called getresponse..................................")
        print(contexts)
        print(text)
        
        // Get request
        let parameters: Parameters = ["phoneNumber": contexts, "text": text]
        
        Alamofire.request(liveBotURL!,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: [:]).responseJSON(completionHandler: { (response) in
            print("after api call")
            if let jsonDictionary = response.result.value as? [String : Any] {
                print(jsonDictionary["sessionId"] as? String)
                print("api inside")
                print(response.result.value)
                
                let chatResponse = ChatResponse()
                chatResponse.sessionId = jsonDictionary["sessionId"] as? String
                chatResponse.contexts = jsonDictionary["contexts"] as? String
                chatResponse.text = jsonDictionary["text"] as? String
               print(chatResponse.sessionId)
                let texttemp = jsonDictionary["text"] as? String
                print(texttemp ?? "Empty msg")
                completion(chatResponse)
            }
            else {
                completion(nil)
            }
        })
        
        
    }
}



