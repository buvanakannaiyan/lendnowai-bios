//
//  shadowView.swift
//  lendnow.ai
//
//  Created by WELCOME on 05/12/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//
import Foundation
import UIKit

class shadowView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.shadowOffset =  CGSize(width: 0, height: 1)   // CGSizeMake(0, 1)
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowRadius = 1.5
        layer.shadowOpacity = 0.65
    }

}
