//
//  Purchase.swift
//  lendnow.ai
//
//  Created by Sundar Gsv on 19/09/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
// 

import Foundation
import ObjectMapper

class Purchase:  NSObject, Mappable
{
    var id: String = ""
    var daysRemain: Int = 0
    var savedAmount: Double = 0.00
    var purcahseAmount: Double = 0.00
    var productName: String = ""
    var imageUrl: String = ""
    var image: UIImage = UIImage()
    var imageExists = false;
    var storeName: String = ""
    var merchantName: String = ""
    var notificationCount: Int = 0
    var purcahseDate: String = ""
    
    var latestNotifications: [Notifications] = []
    var lowerUnitPrices: [Double] = []
    var lowerUnitPriceDates: [Double] = []
    
    override init() {
        super.init()
    }
    
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    /*
     * The below struct is used to represent the model without any problem using Alamofire
     * Eventhough the keys were changed from Back-end. Just change the struct accordingly which
     * prevents changing the all over the code
     */
    
    func mapping(map: Map) {
        
        id <- map["id"]
        daysRemain <- map["daysRemain"]
        savedAmount <- map["savedAmount"]
        purcahseAmount <- map["purcahseAmount"]
        productName <- map["productName"]
        imageUrl <- map["imageUrl"]
        storeName <- map["storeName"]
        merchantName <- map["merchantName"]
        notificationCount <- map["notificationCount"]
        purcahseDate <- map["purcahseDate"]
        latestNotifications <- map["latestNotifications"]
        lowerUnitPrices <- map["lowerUnitPrices"]
        lowerUnitPriceDates <- map["lowerUnitPriceDates"]
        
    }
}
