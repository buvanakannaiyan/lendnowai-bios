//
//  SpeechRecognizerViewController.swift
//  LendNow_iOSClient
//
//  Created by Urmila on 10/23/17.
//  Copyright © 2017 MAC. All rights reserved.
//

import UIKit
import Speech
import AVFoundation



class SpeechRecognizerViewController: UIViewController, SFSpeechRecognizerDelegate, AVSpeechSynthesizerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var chatTableView: UITableView!
    
    
    @IBOutlet weak var chatTextField: UITextField!
    
    let mob: String = ""
    var mob1: String = ""
    
    var recording : Bool = false
    @IBOutlet weak var textprint: UILabel!
    @IBOutlet weak var listenSpeechBtn: UIButton!
    //let cellReuseIdentifier = "personacell"
    
    @IBOutlet weak var fontAwsomebtn: UILabel!
    var audioPlayer: AVAudioPlayer!
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))!
    private let audioEngine = AVAudioEngine()
    
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    var chatHistory: [String] = []
    var chatpersonaHistory: [String] = []
    var chatlivebotHistory: [String] = []
    var chatService: ChatService!
    var contextVar: String = ""
    
    var dataresponse: String = ""
    var res: String = ""
    let pulsator = Pulsator()
    
    var globalpersonarqst: String = ""
    var globalivebotrsponse: String = ""
    
    var livebotimg = #imageLiteral(resourceName: "livebot_profile")
    var personaimg = #imageLiteral(resourceName: "profile")
    
    var globalLabelheight: CGFloat = 0
    var addval: CGFloat = 0
    var d: Bool = true
    @IBOutlet weak var listenLabel: UILabel!
    let synth = AVSpeechSynthesizer()
    var myUtterance = AVSpeechUtterance(string: "")
    
    
    
    @IBAction func listeenButtonTap(_ sender: UIButton) {
        self.d = true
        globalLabelheight = globalLabelheight + addval
        
        //self.printlabelpersona(height: globalLabelheight)
        //  self.printlabelpersona(height: tt)
        //self.printlabellivebot()
        
        recording = !recording
        print(recording)
        print("listen button tapped")
        listenLabel.text = "Listening..."
        if audioEngine.isRunning {
            btnRelease()
            audioEngine.stop()
            d = false
            recognitionRequest?.endAudio()
            listenSpeechBtn.isEnabled = false
            // listenSpeechBtn.setTitle("Ending...", for: .disabled)
            print("Stopped Recording")
            print(globalpersonarqst)
            fontAwsomebtn.textColor = UIColor.black
            //self.printlabelpersona(height: self.globalLabelheight)
            d = true
            chatHistory.append(globalpersonarqst)
            
            print(self.chatHistory)
            
            callAPI(message: dataresponse)
            print(self.chatHistory)
            //chatTableView.reloadData()
            //globalLabelheight = globalLabelheight + addval
        } else {
            if(recording) {
                btnHoldDown()
                try! StartRecording()
                //listenSpeechBtn.setTitle("Stop Recording", for: [])
            }else {
                
                
            }
            
            
        }
        
    }
    /*
     func printlabelpersona(height: CGFloat)
     {
     let screenSize = UIScreen.main.bounds
     //let screenWidth = screenSize.width
     //let screenHeight = screenSize.height
     
     let screenWidth = screenSize.width
     //let numWidth: CGFloat = 3
     //let numHeight: CGFloat = 10
     //self.scrollView.frame.size.width = screenWidth
     //let width: CGFloat = (self.scrollView.frame.size.width - (numWidth + 1))/3
     
     let imageAttachment =  NSTextAttachment()
     imageAttachment.image = self.personaimg
     
     let attachmentString = NSAttributedString(attachment: imageAttachment)
     
     let completeText = NSMutableAttributedString(string: "")
     //Add image to mutable string
     completeText.append(attachmentString)
     //Add your text to mutable string
     //  self.globalpersonarqst = "why my bill went upwhy my bill went upwhy my bill went upwhy my bill went upwhy my bill went upwhy my bill went upwhy my bill went up"
     let  textAfterIcon = NSMutableAttributedString(string: globalpersonarqst)
     
     completeText.append(textAfterIcon)
     
     // let label = UILabel(frame: CGRect(x: 0, y: globalLabelheight, width: self.scrollView.frame.size.width-50, height: globalLabelheight))
     // label.center = CGPoint(x: 160, y: 285)
     label.textAlignment = .left
     label.textColor = UIColor.white
     // label.attributedText(personaimg)
     //label.append.text = "Why my bill went up"
     label.attributedText = completeText;
     label.numberOfLines = 0
     label.sizeToFit()
     label.lineBreakMode = NSLineBreakMode.byWordWrapping
     //NSLayoutConstraint(item: label, attribute: .leading, relatedBy: .equal, toItem: scrollView, attribute: .leadingMargin, multiplier: 1, constant: 10).isActive = true
     
     //self.scrollView.addSubview(label)
     //self.globalLabelheight = self.globalLabelheight + label.frame.height
     addval = label.frame.height
     print(globalLabelheight)
     print("!!!!!!!!!!!!!!!!!!!!plz god!!!!!!!!!!!!!!!!!!!!!!!!")
     }
     
     func printlabellivebot()
     {
     let screenSize = UIScreen.main.bounds
     //let screenWidth = screenSize.width
     //let screenHeight = screenSize.height
     
     let screenWidth = screenSize.width
     //let numWidth: CGFloat = 3
     //let numHeight: CGFloat = 10
     //self.scrollView.frame.size.width = screenWidth
     
     let imageAttachment =  NSTextAttachment()
     imageAttachment.image = #imageLiteral(resourceName: "livebot_profile")
     
     let attachmentString = NSAttributedString(attachment: imageAttachment)
     let completeText = NSMutableAttributedString(string: "")
     //Add image to mutable string
     
     //Add your text to mutable string
     //self.globalivebotrsponse = "why my bill went upwhy my bill went upwhy my bill went upwhy my bill went upwhy my bill went upwhy my bill went upwhy my bill went up"
     let  textAfterIcon = NSMutableAttributedString(string: globalivebotrsponse)
     
     
     completeText.append(textAfterIcon)
     completeText.append(attachmentString)
     // let label = UILabel(frame: CGRect(x: 0, y: globalLabelheight, width: self.scrollView.frame.size.width, height: globalLabelheight))
     // label.center = CGPoint(x: 160, y: 285)
     
     label.textColor = UIColor.white
     // label.attributedText(personaimg)
     //label.append.text = "Why my bill went up"
     label.attributedText = completeText;
     label.textAlignment = .right
     label.numberOfLines = 0
     // label.sizeToFit()
     label.lineBreakMode = NSLineBreakMode.byWordWrapping
     //NSLayoutConstraint(item: label, attribute: .leading, relatedBy: .equal, toItem: scrollView, attribute: .leadingMargin, multiplier: 1, constant: 10).isActive = true
     //self.scrollView.addSubview(label)
     //self.globalLabelheight = self.globalLabelheight + label.frame.height
     addval = label.frame.height
     print(globalLabelheight)
     print("!!!!!!!!!!!!!!!!!!!!plz god!!!!!!!!!!!!!!!!!!!!!!!!")
     
     }
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatHistory.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        chatTableView.rowHeight = 100
        return chatTableView.rowHeight
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //
        print("inside table...................")
        if indexPath.row % 2 == 0 {
            let cell:personaTableViewCell = self.chatTableView.dequeueReusableCell(withIdentifier: "personacell") as! personaTableViewCell
            cell.personaMsg.text = self.chatHistory[indexPath.row]
            cell.personaMsg.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell.personaMsg.numberOfLines = 0
           
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.row % 2 == 1 {
            let cell:livebotTableViewCell = self.chatTableView.dequeueReusableCell(withIdentifier: "livecell") as! livebotTableViewCell
            cell.livebotMsg.text = self.chatHistory[indexPath.row]
            cell.livebotMsg.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell.livebotMsg.numberOfLines = 0
            
            cell.selectionStyle = .none
            print(chatHistory)
            return cell
        }
        
        return UITableViewCell()
    }
    override func viewWillAppear(_ animated: Bool) {
        chatTableView.estimatedRowHeight = 100
        chatTableView.rowHeight = UITableViewAutomaticDimension
        /*let lastSectionIndex = chatTableView.numberOfSections - 1
        
        // Then grab the number of rows in the last section
        let lastRowIndex = chatTableView.numberOfRows(inSection: lastSectionIndex) - 1
        let pathToLastRow = IndexPath(row: lastRowIndex, section: lastSectionIndex)
        //chatTableView.scrollToRowAtIndexPath(pathToLastRow, atScrollPosition: UITableViewScrollPosition.none, animated: true)
        chatTableView.scrollToRow(at: pathToLastRow, at: UITableViewScrollPosition.bottom, animated: true)
 */
    }
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let mob = UserDefaults.standard.string(forKey: "mobile")
//        print(mob)
//
        chatTableView.delegate = self
        chatTableView.dataSource = self
        self.chatTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        // Along with auto layout, these are the keys for enabling variable cell height
        // chatTableView.estimatedRowHeight = 60
        // chatTableView.rowHeight = UITableViewAutomaticDimension
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        //  scrollView.delegate = self
        // scrollView.isScrollEnabled = true
        // scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 7000)
        
        let logoButton  = UIButton(type: .custom)
        logoButton.setImage(UIImage(named: "logo.png"), for: .normal)
        
        listenLabel.isHidden = true
        
        listenSpeechBtn.layer.superlayer?.insertSublayer(pulsator, below: listenSpeechBtn.layer)
        
        
        //let listenLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        //listenLabel.text = "change to red color"
        //listenLabel.textAlignment = .center
        fontAwsomebtn.textColor = UIColor.black
        // let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        //speechSynthesizer.delegate = self
        
        //var p = button.convert(buttonCenter, to: self.view)
        
        chatTextField.delegate = self
        chatTextField.layer.cornerRadius = 18
        
//        chatTextField.returnKeyType = UIReturnKeyType.done
        
        if let storedMobileno = UserDefaults.standard.object(forKey: "mobile")
        {
            print("stored local data")
            print(storedMobileno)
            mob1 = storedMobileno as! String
        }
        else
        {
            print("mobile number does not stored in locally")
            mob1 = "+14088348912"
        }
        
    }
    func btnHoldDown(){
        print("Button hold on")
        // txtpassword.isSecureTextEntry = false
        
        pulsator.animationDuration = 3
        var buttonCenter = CGPoint(x: listenSpeechBtn.bounds.origin.x + listenSpeechBtn.bounds.size.width / 2, y: listenSpeechBtn.bounds.origin.y + listenSpeechBtn.bounds.size.height / 2)
        listenSpeechBtn.layer.addSublayer(pulsator)
        pulsator.position = buttonCenter
        pulsator.numPulse = 3
        pulsator.radius = 240.0
        pulsator.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.06).cgColor
        pulsator.start()
        
        fontAwsomebtn.textColor = UIColor.red
        listenLabel.isHidden = false
        
        ///////////////
        
        
    }
    
    func btnRelease(){
        print("Button release")
        pulsator.stop()
        listenLabel.isHidden = true
        listenSpeechBtn.isEnabled = false
        fontAwsomebtn.textColor = UIColor.black
        self.myUtterance = AVSpeechUtterance(string: globalivebotrsponse)
        self.myUtterance.rate = 0.3
        self.synth.speak(self.myUtterance)
        
        
        
    }
    
    
    func callAPI(message: String)
    {
        print("inside api")
        
        self.chatService = ChatService()
//        if (self.dataresponse == "")
        if (message == "")
        {
            self.d == false
        }
        print("mdksmkmdkjndddddd........................")
        print("data response")
        print(message)
        print("mobile number")
        print(mob1)
        self.chatService.getResponse(contexts: mob1, text: message) { (ChatResponse) in
            if let chatResponse = ChatResponse {
                DispatchQueue.main.async {
                    print("mdksmkmdkjndddddd.........emfkmre")
                    self.chatTextField.text = ""
                    let sessionId = chatResponse.sessionId as? String
                    print("sessionId : \(sessionId)")
                    print(chatResponse.text as? String)
                    
                    self.globalivebotrsponse = (chatResponse.text as? String)!
                    self.chatlivebotHistory.append(self.globalivebotrsponse)
                    //  self.printlabellivebot()
                    self.d = false
                    self.chatHistory.append(self.globalivebotrsponse)
                    if (self.d == true)
                    {
                        print("no words....")
                    }
                    else
                    {
                        self.chatTableView.reloadData()
                    }
                    //self.textprint.text!.append("\n\n")
                    //self.textprint.textAlignment = .right
                    // self.textprint.text!.append((chatResponse.text as? String)!)
                    //  self.textprint.textAlignment = .left
                    
                   // self.contextVar = (chatResponse.contexts as? String)!
                   // print(chatResponse.contexts as? String)
                    
                    //self.res = (chatResponse.text as? String)!
                    
                    
                }
            }
        }
        
    }
    
    
    override public func viewDidAppear(_ animated: Bool) {
        speechRecognizer.delegate = self
        
        SFSpeechRecognizer.requestAuthorization { authStatus in
            
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.listenSpeechBtn.isEnabled = true
                    
                case .denied:
                    self.listenSpeechBtn.isEnabled = false
                    self.listenSpeechBtn.setTitle("User denied access to speech recognition.", for: .disabled)
                    
                case .restricted:
                    self.listenSpeechBtn.isEnabled = false
                    self.listenSpeechBtn.setTitle("Speech recognition restricted on device.", for: .disabled)
                    
                case .notDetermined:
                    self.listenSpeechBtn.isEnabled = false
                    self.listenSpeechBtn.setTitle("Speech recognition not yet authorized.", for: .disabled)
                }
            }
            
            
        }
        
    }
    
    
    private func StartRecording() throws {
        
        if let recognitionTask = recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(AVAudioSessionCategoryRecord)
        try audioSession.setMode(AVAudioSessionModeMeasurement)
        try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let inputNode = audioEngine.inputNode else { fatalError("Audio engine has no input node") }
        guard let recognitionRequest = recognitionRequest else { fatalError("Unable to create a SfSpeechAudioBufferRecognitionRequest object")}
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { result, error in var isFinal = false
            
            
            
            if let result = result {
                
                // self.textprint.text = result.bestTranscription.formattedString
                // self.dataresponse = self.textprint.text!
                
                self.dataresponse = result.bestTranscription.formattedString
                self.globalpersonarqst = self.dataresponse
                self.chatpersonaHistory.append(self.globalpersonarqst)
                // self.chatTableView.reloadData()
                //  self.printlabelpersona(height: self.globalLabelheight)
                self.listenLabel.text = result.bestTranscription.formattedString
                
                print(result.bestTranscription.formattedString)
                
                isFinal = result.isFinal
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.listenSpeechBtn.isEnabled = true
                //self.listenSpeechBtn.setTitle("Start Speaking", for: [])
            }
            
        }
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        try audioEngine.start()
        
        // textfield.text = "I'm listening."
        
    }
    
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            listenSpeechBtn.isEnabled = true
            // listenSpeechBtn.setTitle("Start Recording", for: [])
        } else {
            listenSpeechBtn.isEnabled = false
            // listenSpeechBtn.setTitle("Recognition not available.", for: .disabled)
        }
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    {
        player.stop()
        self.chatService = ChatService()
        self.chatService.getResponse(contexts: "", text: dataresponse) { (ChatResponse) in
            if let resp = ChatResponse {
                DispatchQueue.main.async {
                    let text = resp.text
                    self.globalivebotrsponse = text!
                    self.chatlivebotHistory.append(self.globalpersonarqst)
                    // self.printlabellivebot()
                    
                    
                    //self.textprint.text!.append("\n\n")
                    // self.textprint.text!.append(text!)
                }
            }
        }
        
    }
    
    //MARK:- CALL API WHEN PRESS IQKEYBOARD DONE BUTTON
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print(chatTextField.text)
        print(chatHistory)
        chatHistory.append(chatTextField.text!)
        callAPI(message: chatTextField.text!)
    }
    
    /** func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
       
        print("enter.......")
        print(chatTextField.text)
        print(d)
        print(chatHistory)
        chatHistory.append(chatTextField.text!)
        callAPI(message: chatTextField.text!)
        textField.resignFirstResponder()
        return true
        
    } **/
    
}


