//
//  LendNowProfileViewController.swift
//  lendnow.ai
//
//  Created by WELCOME on 02/06/18.
//  Copyright © 2018 Magnum Opus, Inc. All rights reserved.
//

import UIKit

class LendNowProfileViewController: UIViewController {
    var registeredphonenum: String = ""
    
    
    @IBOutlet weak var firstnameTextField: UITextField!
    
    @IBOutlet weak var lastnameTextField: UITextField!
    
    @IBOutlet weak var mobileTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var tvcbalanceTextField: UITextField!
    
    @IBOutlet weak var outstandingTextField: UITextField!
    
    @IBOutlet weak var requestavailedTextField: UITextField!
    
    var firsttab:String = ""
    
    @IBOutlet weak var profileSkipBtn: UIButton!
    
    @IBOutlet weak var profileContinueBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileContinueBtn.isHidden = true
        
        if let firstTab = UserDefaults.standard.object(forKey: "initialtab")
        {
            print("initial tab")
            print(firstTab)
            firsttab = firstTab as! String
            if firsttab == "profile" {
                profileSkipBtn.isHidden = false
                UserDefaults.standard.set("home", forKey: "initialtab")
            } else {
                profileSkipBtn.isHidden = true
            }
        }

        // Do any additional setup after loading the view.
        if let storedMobileno = UserDefaults.standard.object(forKey: "userPhoneNumber")
        {
            print("stored local data")
            print(storedMobileno)
            registeredphonenum = storedMobileno as! String
            
            firstnameTextField.text = registeredphonenum
            lastnameTextField.text = registeredphonenum
            mobileTextField.text = registeredphonenum
            emailTextField.text = registeredphonenum+"@lendnow.ai"
            
            tvcbalanceTextField.text = "50"
            outstandingTextField.text = "0"
            requestavailedTextField.text = "0" 
        }
        else
        {
            print("mobile number does not stored in locally")
        }
        
        guard let url = URL(string: "http://lendnowai.centralus.cloudapp.azure.com:8080/lendnowai_core/admin/users/borrowers?borrowerPhoneNumber="+registeredphonenum) else { return }
        let session = URLSession.shared
        session.dataTask(with: url) {(data, response, error) in
            if let response = response {
                print("profile response below")
                print(response)
            }
           
            if let data = data {
                print("profile data below")
                print(data)
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print("profile json below")
                    print(json)
                    if let json = json as? [String: Any] {
                        for (key, value) in json {
                            print("key value are below---->")
                            print(key)
                            print(value)
                            
                         /**   if key == "tcvBalance" {
                                self.tvcbalanceTextField.text = "\(value)"
                            }
                            if key == "outstandingBalanceDue" {
                                self.outstandingTextField.text = value as! String
                            } **/
                            /**if key == "user" {
                                for item in value {
                                    //Do stuff
                                    print("items are below")
                                    print(item)
                                }
                            } **/
                        }
                    }
                } catch {
                    print("profile error")
                    print(error)
                }
            }
        }.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark:- Handle skip button clicked
    @IBAction func skipBtnTapped(_ sender: Any) {
        profileSkipBtn.isHidden = false
        UserDefaults.standard.set("home", forKey: "initialtab")
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "transactionDetailToTab") as! tabBarViewController
        vc.selectedIndex = 0
        self.present(vc, animated: true, completion: nil)
    }
    

}
