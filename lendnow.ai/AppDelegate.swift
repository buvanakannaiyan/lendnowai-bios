//
//  AppDelegate.swift
//  lendnow.ai
//
//  Created by Sundar Gsv on 15/09/17.
//  Copyright © 2017 Magnum opus, Inc. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UserNotifications
import Firebase
import RealmSwift
import SwiftyJSON

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    // Mark : The below lines are related to cache initialization
    let appSettings = UserDefaults.standard
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.sharedManager().enable = true
        
        if let userPhoneNumber = UserDefaults.standard.value(forKey: "userPhoneNumber"){
            // print(userPhoneNumber)
            
            /* Start Caching mechanism */
            
//            print("Skip Caching")
             /* End Caching mechanism */
            
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "AuthVC")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
            
        } else {
            print("Register now")
        }
        
        
        FirebaseApp.configure()
        
        // [START set_messaging_delegate]
//        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        

        
        return true
    }
    
    // [START receive_InitialTransaction]

    
    // END receive_InitialTransaction]
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print("didReceiveRemoteNotification called : ")
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        print("didReceiveRemoteNotification called on ios 10 ")
        // Print message ID.
         if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler(.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        print("didReceiveRemoteNotification called onn ios 10 diff ")

         if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
    
}
// [END ios_10_message_handling]



extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        if let userPhoneNumber = UserDefaults.standard.value(forKey: "userPhoneNumber"){
            var loggedInPhoneNumber = userPhoneNumber as! String
//            var token = UserDefaults.standard.value(forKey: "userToken")
                refreshToken(fcmToken:  fcmToken, loggedInPhoneNumber: loggedInPhoneNumber)
            
        }
       
    }
    // [END refresh_token]
    
    func refreshToken(fcmToken: String, loggedInPhoneNumber: String) {
        print("refreshing token")
        // MARK : To delete the token
        let instance = InstanceID.instanceID()
        _ = InstanceID.deleteID(instance)
        InstanceID.instanceID().deleteID { (err:Error?) in
            if err != nil{
                print(err.debugDescription);
            } else {
                print("Token Deleted");
                self.appSettings.set("", forKey: "userToken")
                print("DELETED TOKEN : \(UserDefaults.standard.value(forKey: "userToken")!)")
                self.genFCMToken(fcmToken:  fcmToken, loggedInPhoneNumber: loggedInPhoneNumber)
                
            }
        }
    }
    
    func genFCMToken(fcmToken: String, loggedInPhoneNumber: String) {
        var firebaseService : GeneralService!
        firebaseService = GeneralService()
        firebaseService.updateFCM(userPhoneNumber: loggedInPhoneNumber, token: fcmToken) { (res) in
            
            if let message = res {
                DispatchQueue.main.async {
                    
                    if message as! String == "Token updated successful" {
                        print("Token added/ updated")
                        self.appSettings.set(fcmToken, forKey: loggedInPhoneNumber)
                        print("NEW TOKEN : \(UserDefaults.standard.value(forKey: "userToken")!)")
                    } else {
                        print("Token not updated")
                    }
                    
                }
            }
        }
        
    }

    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
    
    // [The below function's are default]

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}




/** class AppDelegate: UIResponder, UIApplicationDelegate {
 
 var window: UIWindow?
 let appSettings = UserDefaults.standard
 
 func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
 // Override point for customization after application launch.
 IQKeyboardManager.sharedManager().enable = true
 
 if let userPhoneNumber = UserDefaults.standard.value(forKey: "userPhoneNumber"){
 
 self.window = UIWindow(frame: UIScreen.main.bounds)
 let storyboard = UIStoryboard(name: "Main", bundle: nil)
 let initialViewController = storyboard.instantiateViewController(withIdentifier: "AuthVC")
 self.window?.rootViewController = initialViewController
 self.window?.makeKeyAndVisible()
 
 } else {
 print("Register now")
 }
 
 return true
 }
 
 func applicationWillResignActive(_ application: UIApplication) {
 // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
 // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
 }
 
 func applicationDidEnterBackground(_ application: UIApplication) {
 // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
 // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
 }
 
 func applicationWillEnterForeground(_ application: UIApplication) {
 // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
 }
 
 func applicationDidBecomeActive(_ application: UIApplication) {
 // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
 }
 
 func applicationWillTerminate(_ application: UIApplication) {
 // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
 }
 
 
 } **/
 
 


