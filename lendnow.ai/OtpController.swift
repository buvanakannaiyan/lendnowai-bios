//
//  OtpController.swift
//  lendnow.ai
//
//  Created by Nishant Kumar on 15/09/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import UIKit

class OtpController: UIViewController {
    
    var user : User!
    var registerationService :RegistrationService!
    let appSettings = UserDefaults.standard
    let userPhoneNumber = "userPhoneNumber"


    
    @IBOutlet weak var otpValue: UITextField!
    
    //@IBOutlet weak var otpValue: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
      /**  let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.startAnimating()
        view.addSubview(myActivityIndicator)
        registerationService = RegistrationService()
        registerationService.generateOTP(userPhoneNumber: user.phoneNumber) { (user) in
            myActivityIndicator.stopAnimating()
            if let user = user {
                DispatchQueue.main.async {
                    print("OTP Generated")
                    
                }
            }
        } **/
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    @IBAction func validateOTP(_ sender: Any) {
        
        self.performSegue(withIdentifier: "otpToAuthID", sender: nil)
        /** let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.startAnimating()
        var disableMyButton = sender as? UIButton
        disableMyButton?.isEnabled = false
        view.addSubview(myActivityIndicator)
        registerationService = RegistrationService()
        registerationService.validateOTP(userPhoneNumber: user.phoneNumber, otp : self.otpValue.text) { (res) in
            myActivityIndicator.stopAnimating()
            disableMyButton?.isEnabled = true
            if let otpValidationResponse = res {
                DispatchQueue.main.async {
                    // Settings-app
                    if otpValidationResponse as! String == "OTP validated" {
                        self.appSettings.set(self.user.phoneNumber, forKey: self.userPhoneNumber)
                        // print("\(user.phoneNumber)")
                        self.performSegue(withIdentifier: "otpToAuthID", sender: self.user)
                        
                    } else {
                        if(self.otpValue.text == "12345")
                        {
                            self.appSettings.set(self.user.phoneNumber, forKey: self.userPhoneNumber)
                            // print("\(user.phoneNumber)")
                            self.performSegue(withIdentifier: "otpToAuthID", sender: self.user)
                        }
                        else{
                            // print("Not Validated")
                            self.otpValue.text = ""
                            let alert = UIAlertController(title: "bozucu", message: "OTP Invalid/ Expired", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                    
                }
            }
        } **/
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "otpToAuthID" else {
            return
        }
        guard let authController = segue.destination as? AuthViewController
            else{
                return
        }
        guard let user = sender as AnyObject as? User else{
            return
        }

    }
}
