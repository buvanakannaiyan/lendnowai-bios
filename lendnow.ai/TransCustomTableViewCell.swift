//
//  TransCustomTableViewCell.swift
//  lendnow.ai
//
//  Created by Urmila on 11/10/17.
//  Modified by Sundar Gsv on 26/10/17
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import UIKit
import Charts
import Cloudinary
//import SwiftChart


class TransCustomTableViewCell: UITableViewCell,UIWebViewDelegate {
    
    let cloudinary = CLDCloudinary(configuration: CLDConfiguration(cloudinaryUrl: "cloudinary://341447472638319:OAkljZsMuPUhdPZ8TZ54OdWM-ok@dzp9fse75")!)
    var imageExists = false
    @IBOutlet weak var segmentview: UISegmentedControl!
   
    @IBOutlet weak var dayslabel: UILabel!
    @IBOutlet weak var detLabel: UILabel!

    @IBOutlet weak var notificationImage: UIImageView!
    @IBOutlet weak var notificationCount: UILabel!
    @IBOutlet weak var purchaseAmount: UILabel!
    
   var globalnotifycount = 0

    var productName: String = ""
    
    var storeSegmentView: UIView!
    var notificationSegmentView: UIView!
    var chartSegmentView: UIView!
    let screenSize = UIScreen.main.bounds
   
    @IBOutlet weak var storeView: UIView!
    var item = Purchase()
   
var findSeries: [String] = []
    
// create a 1x1 image with this color
private func imageWithColor(color: UIColor) -> UIImage {
    let rect = CGRect(x: 0.0, y: 0.0, width:  1.0, height: 1.0)
    UIGraphicsBeginImageContext(rect.size)
    let context = UIGraphicsGetCurrentContext()
    context!.setFillColor(color.cgColor);
    context!.fill(rect);
    let image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image!
}
    func exponentize(str: String) -> String {
        
        let supers = [
            "0": "\u{2070}",
            "1": "\u{00B9}",
            "2": "\u{00B2}",
            "3": "\u{00B3}",
            "4": "\u{2074}",
            "5": "\u{2075}",
            "6": "\u{2076}",
            "7": "\u{2077}",
            "8": "\u{2078}",
            "9": "\u{2079}"]
        
        var newStr = ""
        var isExp = false
        for (_, char) in str.characters.enumerated() {
            if char == "." {
                isExp = true
            } else {
                if isExp {
                    let key = String(char)
                    if supers.keys.contains(key) {
                        newStr.append(Character(supers[key]!))
                    } else {
                        isExp = false
                        newStr.append(char)
                    }
                } else {
                    newStr.append(char)
                }
            }
        }
        return newStr
    }
    func setCell(item: Purchase)
    {
        self.cloudinary.cachePolicy = CLDImageCachePolicy.disk
        self.cloudinary.cacheMaxDiskCapacity = 50 * 1024 * 1024

        
        
          segmentview.setDividerImage(imageWithColor(color: UIColor.white), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
        
         segmentview.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: UIControlState.normal)
        segmentview.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: UIControlState.selected)
        segmentview.selectedSegmentIndex = 0
       // notificationCount.layer.backgroundColor  = UIColor.red.cgColor
        notificationCount.layer.masksToBounds = true
        notificationCount.layer.cornerRadius = 7
        self.notificationImage.image = UIImage(named: "dis_bell")
        self.notificationCount.isHidden  = true
       
     var attString = "$"
        attString.append(exponentize(str: "\(item.savedAmount)"))
        dayslabel.text = "\(item.daysRemain)"
      
        detLabel.text = item.productName
  
        purchaseAmount.text = attString
        globalnotifycount = item.notificationCount
        if (item.notificationCount > 0) {
            self.notificationImage.image = UIImage(named: "bell")
            self.notificationCount.isHidden  = false
            notificationCount.text = String(item.notificationCount)
        }
        
      
        self.item = item
        self.productName = item.productName
        segmentAct(sender: Any.self)
        // setstoreview(item: self.item)
        
    }
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func buttonAction1(sender: UIButton!) {
//        print("button 1")
        if(findSeries.contains("series1")){
             sender.setTitle("+", for: .normal)
            findSeries = findSeries.filter{$0 != "series1"}
            
            
        }else{
            findSeries.append("series1")
            sender.setTitle("x", for: .normal)
        }
        
        initializeChart("notAll")
        
    }
    func buttonAction2(sender: UIButton!) {
//          print("button 2")
        if(findSeries.contains("series2")){
            sender.setTitle("+", for: .normal)
            findSeries = findSeries.filter{$0 != "series2"}
        }else{
            findSeries.append("series2")
            sender.setTitle("x", for: .normal)
        }
        
        initializeChart("notAll")
        
    }
    func buttonAction3(sender: UIButton!) {
//          print("button 3")
        if(findSeries.contains("series3")){
            sender.setTitle("+", for: .normal)
            findSeries = findSeries.filter{$0 != "series3"}
        }else{
            findSeries.append("series3")
            sender.setTitle("x", for: .normal)
        }
        
        initializeChart("notAll")
        
    }
    func buttonAction4(sender: UIButton!) {
//          print("button 4")
        if(findSeries.contains("series4")){
            sender.setTitle("+", for: .normal)
            findSeries = findSeries.filter{$0 != "series4"}
        }else{
            findSeries.append("series4")
            sender.setTitle("x", for: .normal)
        }
        
        initializeChart("notAll")
        
    }
    func buttonAction5(sender: UIButton!) {
//        print("button 5")
        if(findSeries.contains("series5")){
            sender.setTitle("+", for: .normal)
            findSeries = findSeries.filter{$0 != "series5"}
        }else{
            findSeries.append("series5")
            sender.setTitle("x", for: .normal)
        }
        
        initializeChart("notAll")
        
    }
    func initializeChart(_ line_to_show:String) {
        
    
        var lineView = LineChartView.init(frame:  CGRect(x: 20, y: 20, width: storeView.frame.width-150, height: 150))
         var months = ["11/11","12/11","13/11","14/11"]
        //Change the  chart layout
        lineView.xAxis.labelPosition = .bottom
        lineView.legend.enabled = false
        lineView.rightAxis.enabled = false
        lineView.xAxis.drawAxisLineEnabled = false
        lineView.leftAxis.axisMinimum = 0
        //barView.xAxis.setLabelCount(dataPoints.count, force: true)
        lineView.xAxis.drawGridLinesEnabled = false
        lineView.chartDescription?.text = ""
        // barView.xAxis.axisMinimum = -0.5;
        //barView.xAxis.axisMaximum = Double(dataPoints.count) - 0.5;
        lineView.xAxis.valueFormatter = IndexAxisValueFormatter(values:months)
        lineView.xAxis.granularityEnabled = true
        lineView.xAxis.granularity = 1
        lineView.xAxis.wordWrapEnabled=true
        lineView.xAxis.avoidFirstLastClippingEnabled = false
        lineView.xAxis.wordWrapEnabled = false
        lineView.leftAxis.drawGridLinesEnabled = false
        lineView.leftAxis.drawZeroLineEnabled = true
        lineView.rightAxis.drawZeroLineEnabled=true
        lineView.clearAllViewportJobs()
 
        var allLineChartDataSets: [LineChartDataSet] = [LineChartDataSet]()
        
        /*****first Line****/
        var lineChartEntry = [ChartDataEntry]()
        var dataPoints = [3,3,3,3]
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: Double(dataPoints[i]))
            lineChartEntry.append(dataEntry)
        }
        let chartDataSet:LineChartDataSet = LineChartDataSet(values: lineChartEntry, label: "Date")
        chartDataSet.mode = .cubicBezier
        chartDataSet.colors = [UIColor.red]
        chartDataSet.drawCirclesEnabled = false
        
       
        /******Second Line********/
        
        var lineChartEntry2 = [ChartDataEntry]()
        var dataPoints2 = [4,2,4,2]
        
        for i in 0..<dataPoints2.count {
            let dataEntry2 = ChartDataEntry(x: Double(i), y: Double(dataPoints2[i]))
            lineChartEntry2.append(dataEntry2)
        }
        let chartDataSet2:LineChartDataSet = LineChartDataSet(values: lineChartEntry2,label:nil)
        chartDataSet2.mode = .cubicBezier
        chartDataSet2.colors = [UIColor(red: 0, green: 0.8, blue: 0.9765, alpha: 1.0)]
        chartDataSet2.drawCirclesEnabled = false
        
      
        /******Third Line********/
        
        var lineChartEntry3 = [ChartDataEntry]()
        var dataPoints3 = [2,4,2,4]
        
        for i in 0..<dataPoints3.count {
            let dataEntry3 = ChartDataEntry(x: Double(i), y: Double(dataPoints3[i]))
            lineChartEntry3.append(dataEntry3)
        }
        let chartDataSet3:LineChartDataSet = LineChartDataSet(values: lineChartEntry3,label:nil)
        chartDataSet3.mode = .cubicBezier
        chartDataSet3.colors = [UIColor(red: 0.4745, green: 0, blue: 0.5686, alpha: 1.0)]
        chartDataSet3.drawCirclesEnabled = false
        
      
        /******fourth Line********/
        
        var lineChartEntry4 = [ChartDataEntry]()
        var dataPoints4 = [1,3,1,3]
        
        for i in 0..<dataPoints4.count {
            let dataEntry4 = ChartDataEntry(x: Double(i), y: Double(dataPoints4[i]))
            lineChartEntry4.append(dataEntry4)
        }
        let chartDataSet4:LineChartDataSet = LineChartDataSet(values: lineChartEntry4,label:nil)
        chartDataSet4.mode = .cubicBezier
        chartDataSet4.colors = [UIColor(red: 0.2392, green: 0.5765, blue: 0, alpha: 1.0)]
        chartDataSet4.drawCirclesEnabled = false
        
      
        /******Fifth Line********/
        
        var lineChartEntry5 = [ChartDataEntry]()
        var dataPoints5 = [5,2,5,2]
        
        for i in 0..<dataPoints5.count {
            let dataEntry5 = ChartDataEntry(x: Double(i), y: Double(dataPoints5[i]))
            lineChartEntry5.append(dataEntry5)
        }
        let chartDataSet5:LineChartDataSet = LineChartDataSet(values: lineChartEntry5,label:nil)
        chartDataSet5.mode = .cubicBezier
        chartDataSet5.colors = [UIColor(red: 0.9176, green: 0.9686, blue: 0, alpha: 1.0)]
        chartDataSet5.drawCirclesEnabled = false
       
        
        /******put all the line together*****/
       
       
        
        
        

        if(line_to_show == "all"){
            if (item.notificationCount == 0)
            {
                findSeries.append("series1")
                allLineChartDataSets.append(chartDataSet)
                
            }
            else
            {
                findSeries.append("series1")
                findSeries.append("series2")
                findSeries.append("series3")
                findSeries.append("series4")
                findSeries.append("series5")
                allLineChartDataSets.append(chartDataSet)
                allLineChartDataSets.append(chartDataSet5)
                allLineChartDataSets.append(chartDataSet2)
                allLineChartDataSets.append(chartDataSet3)
                allLineChartDataSets.append(chartDataSet4)
                
                
            }
            
        }else{
            //  print(findSeries)
            for v in storeView.subviews{
                if v is Charts.LineChartView{
                    v.removeFromSuperview()
                }
                
            }
           
            
            if ( findSeries.contains( "series1" ) ) {
                 allLineChartDataSets.append(chartDataSet)
                
            }
            if ( findSeries.contains( "series2" ) ) {
                 allLineChartDataSets.append(chartDataSet2)
                
            }
            if ( findSeries.contains( "series3" ) ) {
                 allLineChartDataSets.append(chartDataSet3)
                
            }
            if ( findSeries.contains( "series4" ) ) {
                 allLineChartDataSets.append(chartDataSet4)
                
            }
            if ( findSeries.contains( "series5" ) ) {
                 allLineChartDataSets.append(chartDataSet5)
                
            }
        }
        
        let chartData = LineChartData( dataSets: allLineChartDataSets)
        chartData.setDrawValues(false)
        lineView.data = chartData
        storeView.addSubview(lineView)
       
        
    }
    
    func updateGraph() {
         let screenWidth = screenSize.width
        if (item.notificationCount == 0)
        {
        let myView = UIView(frame: CGRect(x: screenWidth-130, y: 10, width: 100, height: 20))
        myView.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
        
        let color1 = UIView(frame: CGRect(x: 3, y: 3, width: 10, height: 14))
        color1.backgroundColor = UIColor(red: 229/255, green: 49/255, blue: 49/255, alpha: 1.0)
        myView.addSubview(color1)
        
        let colorlabel1 = UILabel(frame: CGRect(x: 15, y: 3, width: 70, height: 14))
        colorlabel1.text = "My Purchase"
        colorlabel1.font = colorlabel1.font.withSize(10)
        myView.addSubview(colorlabel1)
        
        let colorbuton1 = UIButton(frame: CGRect(x: 0, y: 0, width: 95, height: 20))
        colorbuton1.setTitle("x", for: .normal)
        colorbuton1.titleLabel?.font.withSize(8)
        colorbuton1.backgroundColor = UIColor.clear
        colorbuton1.contentHorizontalAlignment = .right
        colorbuton1.setTitleColor(UIColor.black, for: .normal)
        colorbuton1.addTarget(self, action: #selector(buttonAction1), for: .touchUpInside)
        
        myView.addSubview(colorbuton1)
        storeView.addSubview(myView)
        }
        else
        {
            let myView = UIView(frame: CGRect(x: screenWidth-130, y: 10, width: 100, height: 20))
            myView.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
            
            let color1 = UIView(frame: CGRect(x: 3, y: 3, width: 10, height: 14))
            color1.backgroundColor = UIColor(red: 229/255, green: 49/255, blue: 49/255, alpha: 1.0)
            myView.addSubview(color1)
            
            let colorlabel1 = UILabel(frame: CGRect(x: 15, y: 3, width: 70, height: 14))
            colorlabel1.text = "My Purchase"
            colorlabel1.font = colorlabel1.font.withSize(10)
            myView.addSubview(colorlabel1)
            
            let colorbuton1 = UIButton(frame: CGRect(x: 0, y: 0, width: 95, height: 20))
            colorbuton1.setTitle("x", for: .normal)
            colorbuton1.titleLabel?.font.withSize(8)
            colorbuton1.backgroundColor = UIColor.clear
            colorbuton1.contentHorizontalAlignment = .right
            colorbuton1.setTitleColor(UIColor.black, for: .normal)
            colorbuton1.addTarget(self, action: #selector(buttonAction1), for: .touchUpInside)
            
            myView.addSubview(colorbuton1)
            storeView.addSubview(myView)
        
        let myView1 = UIView(frame: CGRect(x: screenWidth-130, y: 35, width: 100, height: 20))
        myView1.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
       
        let color2 = UIView(frame: CGRect(x: 3, y: 3, width: 10, height: 14))
        color2.backgroundColor = UIColor(red: 0/255, green: 191/255, blue: 243/255, alpha: 1.0)
        myView1.addSubview(color2)
        
        let colorlabel2 = UILabel(frame: CGRect(x: 15, y: 3, width: 70, height: 14))
        colorlabel2.text = "Store"
        colorlabel2.font = colorlabel2.font.withSize(10)
        myView1.addSubview(colorlabel2)
        
        let colorbuton2 = UIButton(frame: CGRect(x: 0, y: 0, width: 95, height: 20))
        colorbuton2.setTitle("x", for: .normal)
        colorbuton2.backgroundColor = UIColor.clear
        colorbuton2.contentHorizontalAlignment = .right
        colorbuton2.setTitleColor(UIColor.black, for: .normal)
        colorbuton2.addTarget(self, action: #selector(buttonAction2), for: .touchUpInside)
         myView1.addSubview(colorbuton2)
        
        
        let myView2 = UIView(frame: CGRect(x: screenWidth-130, y: 60, width: 100, height: 20))
        myView2.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
        
        let color3 = UIView(frame: CGRect(x: 3, y: 3, width: 10, height: 14))
        color3.backgroundColor = UIColor(red: 146/255, green: 39/255, blue: 143/255, alpha: 1.0)
        myView2.addSubview(color3)
        
        let colorlabel3 = UILabel(frame: CGRect(x: 15, y: 3, width: 70, height: 14))
        colorlabel3.text = "Zipcode"
        colorlabel3.font = colorlabel3.font.withSize(10)
        myView2.addSubview(colorlabel3)
        
        let colorbuton3 = UIButton(frame: CGRect(x: 0, y: 0, width: 95, height: 20))
        colorbuton3.setTitle("x", for: .normal)
        colorbuton3.backgroundColor = UIColor.clear
        colorbuton3.contentHorizontalAlignment = .right
        colorbuton3.setTitleColor(UIColor.black, for: .normal)
        colorbuton3.addTarget(self, action: #selector(buttonAction3), for: .touchUpInside)
         myView2.addSubview(colorbuton3)
        
        
        let myView4 = UIView(frame: CGRect(x: screenWidth-130, y: 85, width: 100, height: 20))
        myView4.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
        
        let color5 = UIView(frame: CGRect(x: 3, y: 3, width: 10, height: 14))
        color5.backgroundColor = UIColor(red: 0/255, green: 153/255, blue: 0/255, alpha: 1.0)
        myView4.addSubview(color5)
        
        let colorlabel5 = UILabel(frame: CGRect(x: 15, y: 3, width: 70, height: 14))
        colorlabel5.text = "Merchant"
        colorlabel5.font = colorlabel5.font.withSize(10)
        myView4.addSubview(colorlabel5)
        
        let colorbuton5 = UIButton(frame: CGRect(x: 0, y: 0, width: 95, height: 20))
        colorbuton5.setTitle("x", for: .normal)
        colorbuton5.backgroundColor = UIColor.clear
        colorbuton5.contentHorizontalAlignment = .right
        colorbuton5.setTitleColor(UIColor.black, for: .normal)
        colorbuton5.addTarget(self, action: #selector(buttonAction4), for: .touchUpInside)
        myView4.addSubview(colorbuton5)
        
 
        let myView5 = UIView(frame: CGRect(x: screenWidth-130, y: 110, width: 100, height: 20))
        myView5.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
        
        let color6 = UIView(frame: CGRect(x: 3, y: 3, width: 10, height: 14))
        color6.backgroundColor = UIColor.yellow
        myView5.addSubview(color6)
        
        let colorlabel6 = UILabel(frame: CGRect(x: 15, y: 3, width: 70, height: 14))
        colorlabel6.text = "National"
        colorlabel6.font = colorlabel6.font.withSize(10)
        myView5.addSubview(colorlabel6)
        
        let colorbuton6 = UIButton(frame: CGRect(x: 0, y: 0, width: 95, height: 20))
        colorbuton6.setTitle("x", for: .normal)
        colorbuton6.backgroundColor = UIColor.clear
        colorbuton6.contentHorizontalAlignment = .right
        colorbuton6.setTitleColor(UIColor.black, for: .normal)
        colorbuton6.addTarget(self, action: #selector(buttonAction5), for: .touchUpInside)
        myView5.addSubview(colorbuton6)
        
       
        storeView.addSubview(myView)
        storeView.addSubview(myView1)
        storeView.addSubview(myView2)
        storeView.addSubview(myView4)
        storeView.addSubview(myView5)
        }
       initializeChart("all")
        
    }
    
    func setstoreview(item: Purchase)
    {
        let labeldate1 = UILabel(frame: CGRect(x: 20, y: 21, width: storeView.frame.width/4-10, height: 20))
        labeldate1.text = "Date"
        labeldate1.font = UIFont(name: "MyriadPro-Regular", size: 10)
        labeldate1.font = labeldate1.font.withSize(12)
        labeldate1.textAlignment = .left
        labeldate1.textColor = UIColor.gray
        
         let label2 = UILabel(frame: CGRect(x: storeView.frame.width/4-20, y: 20, width: storeView.frame.width/4-10, height: 20))
         label2.text = "Merchant"
         label2.textAlignment = .left
         label2.textColor = UIColor.gray
         label2.font = label2.font.withSize(12)
//         print(storeView.frame.width-78)
        
        let labelprice = UILabel(frame: CGRect(x: storeView.frame.width/4+70, y: 20, width: storeView.frame.width/4+30, height: 20))
        labelprice.text = "Purchase Price"
        labelprice.textColor = UIColor.gray
        labelprice.textAlignment = .left
        labelprice.font = UIFont(name: "MyriadPro-Regular", size: 10)
        labelprice.font = labelprice.font.withSize(12)
        
        
        let singleTap = UITapGestureRecognizer(target: self, action: Selector("productimageTab"))
        singleTap.numberOfTapsRequired = 1
        let screenWidth = screenSize.width
        
        var prodImg = UIImageView(frame:CGRect(x: (screenWidth-130),y: 21,width: 100, height: 100))
        // MARK :_ Product image
        if(item.imageUrl != ""){
            if(!item.imageExists){
                print("imageExists is false")
                print(item.imageUrl)
                let myImage = cloudinary.createDownloader().fetchImage(item.imageUrl, { (totalBytesExpected) in
                    // Handle progress
                    //add smallActivityIndicators
                    print("progressing...")
                }) { (responseImage, error) in
                    if(responseImage != nil){
                        prodImg.image = responseImage
                        item.image = responseImage!
                        item.imageExists = true
                    }else{
                        // set default image
                        prodImg.image = UIImage(named: "product-1.png")
                    }
                    // responseImage is an instance of UIImage
                    // error is an instance of NSError
                }
                
            }else{
                print("imageExists is true")
                prodImg.image = item.image
            }
        }else{
            // set default image
            prodImg.image = UIImage(named: "product-1.png")

        }
        
        prodImg.isUserInteractionEnabled = true
        prodImg.addGestureRecognizer(singleTap)
        
    
        
        let labeldate = UILabel(frame: CGRect(x: 20, y: 41, width: storeView.frame.width/4-10, height: 62))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let myDate = dateFormatter.date(from: item.purcahseDate)!
        
        dateFormatter.dateFormat = "MM/dd"
        let somedateString = dateFormatter.string(from: myDate)
        
        labeldate.text = somedateString
        labeldate.sizeToFit()
        labeldate.numberOfLines = 0
        labeldate.textAlignment = .left
        labeldate.font = UIFont(name: "MyriadPro-Regular", size: 15)
        labeldate.textColor = UIColor(red: 7/255, green: 49/255, blue: 118/255, alpha: 1.0)
        
        let label22 = UILabel(frame: CGRect(x: storeView.frame.width/4-20, y: 40, width: storeView.frame.width/4, height: 20))
         label22.text = item.merchantName
        label22.sizeToFit()
        label22.numberOfLines = 0
         label22.textAlignment = .left
         label22.font = label22.font.withSize(15)
         label22.numberOfLines = 0
         label22.textColor = UIColor(red: 7/255, green: 49/255, blue: 118/255, alpha: 1.0)
        
        let labelamount = UILabel(frame: CGRect(x: storeView.frame.width/4+70, y: 40, width: storeView.frame.width/4-10, height: 62))
        
        //labelamount.attributedText = attStringa
        labelamount.text = "$ \(item.purcahseAmount)"
        labelamount.textAlignment = .left
        labelamount.textColor = UIColor.red
        labelamount.sizeToFit()
        labelamount.numberOfLines = 0
        labelamount.font = UIFont(name: "MyriadPro-Regular", size: 15)
        
        let labelstname = UILabel(frame: CGRect(x: 20, y: 70, width: storeView.frame.width/2, height: 20))
        labelstname.text = "Store Name"
        
        labelstname.numberOfLines = 0
        labelstname.textColor = UIColor.gray
        labelstname.font = UIFont(name: "MyriadPro-Regular", size: 10)
        labelstname.font = labelstname.font.withSize(12)
        let labelstname1 = UILabel(frame: CGRect(x: 20, y: 90, width: storeView.frame.width-130, height: 75))
        if(item.storeName == "")
        {
            labelstname1.text = "ST# 02119"
        }
        else{
        labelstname1.text = item.storeName
        }
        labelstname1.textAlignment = .left
        labelstname1.sizeToFit()
        labelstname1.numberOfLines = 0
        
        labelstname1.textColor = UIColor(red: 7/255, green: 49/255, blue: 118/255, alpha: 1.0)
        labelstname1.font = UIFont(name: "MyriadPro-Regular", size: 15)
        
        let labelproddet = UILabel(frame: CGRect(x: 20, y: 120, width: storeView.frame.width/2, height: 20))
        labelproddet.text = "Store Details"
        
        labelproddet.numberOfLines = 0
        labelproddet.textColor = UIColor.gray
        labelproddet.font = UIFont(name: "MyriadPro-Regular", size: 10)
        labelproddet.font = labelproddet.font.withSize(12)
        let labelproddet1 = UILabel(frame: CGRect(x: 20, y: 140, width: storeView.frame.width-130, height: 75))
        labelproddet1.text = "Monroe, LA 78712."
        labelproddet1.textAlignment = .left
        labelproddet1.sizeToFit()
        labelproddet1.numberOfLines = 0
        
        labelproddet1.textColor = UIColor(red: 7/255, green: 49/255, blue: 118/255, alpha: 1.0)
        labelproddet1.font = UIFont(name: "MyriadPro-Regular", size: 15)
        
        storeView.addSubview(labeldate1)
        storeView.addSubview(label2)
        storeView.addSubview(labelprice)
        storeView.addSubview(labeldate)
        storeView.addSubview(label22)
        storeView.addSubview(labelamount)
        storeView.addSubview(prodImg)
        storeView.addSubview(labelstname)
        storeView.addSubview(labelstname1)
        storeView.addSubview(labelproddet)
        storeView.addSubview(labelproddet1)
    }
    func setnotificationview(item: Purchase)
    {
        
        let label1 = UILabel(frame: CGRect(x: 20, y: 10, width: storeView.frame.width/4-10, height: 21))
        label1.text = "Date"
        label1.font = UIFont(name: "MyriadPro-Regular", size: 10)
        label1.font = label1.font.withSize(12)
        label1.textAlignment = .left
        label1.textColor = UIColor.gray
        let label2 = UILabel(frame: CGRect(x: storeView.frame.width/4-20, y: 10, width: storeView.frame.width/4+10, height: 21))
        label2.text = "Store Details"
        label2.textAlignment = .left
        label2.textColor = UIColor.gray
        label2.font = UIFont(name: "MyriadPro-Regular", size: 10)
        label2.font = label2.font.withSize(12)
        let label3 = UILabel(frame: CGRect(x: (2*storeView.frame.width/4)+30, y: 10, width: storeView.frame.width/4+10, height: 21))
        label3.text = "Amount"
        label3.textColor = UIColor.gray
        label3.textAlignment = .left
        label3.font = UIFont(name: "MyriadPro-Regular", size: 10)
        label3.font = label3.font.withSize(12)
        let label3def = UILabel(frame: CGRect(x: (3*storeView.frame.width/4)+10, y: 10, width: storeView.frame.width/4+10, height: 21))
        label3def.text = "Difference"
        label3def.textColor = UIColor.gray
        label3def.textAlignment = .left
        label3def.font = UIFont(name: "MyriadPro-Regular", size: 10)
        label3def.font = label3def.font.withSize(12)
        let label11 = UILabel(frame: CGRect(x: 20, y: 15, width: storeView.frame.width/4-10, height: 62))
//        print(item.latestNotifications)
        //label11.text = item.latestNotifications[0].purchaseDate
        label11.textAlignment = .left
        label11.font = UIFont(name: "MyriadPro-Regular", size: 15)
        label11.textColor = UIColor(red: 7/255, green: 49/255, blue: 118/255, alpha: 1.0)
        let label22 = UILabel(frame: CGRect(x: storeView.frame.width/4-20, y: 15, width: storeView.frame.width/4+10, height: 62))
        //label22.text = item.latestNotifications[0].merchantName
        label22.textAlignment = .left
        label22.font = UIFont(name: "MyriadPro-Regular", size: 15)
        label22.numberOfLines = 0
        label22.textColor = UIColor(red: 7/255, green: 49/255, blue: 118/255, alpha: 1.0)
        let label33 = UILabel(frame: CGRect(x: (2*storeView.frame.width/4)+30, y: 15, width: storeView.frame.width/4-10, height: 62))
        //label33.text = "\(item.latestNotifications[0].unitPrice)"
        label33.textAlignment = .left
        label33.textColor = UIColor(red: 7/255, green: 49/255, blue: 118/255, alpha: 1.0)
        label33.font = UIFont(name: "MyriadPro-Regular", size: 15)
        
        let label33def = UILabel(frame: CGRect(x: (3*storeView.frame.width/4)+10, y: 15, width: storeView.frame.width/4-10, height: 62))
       // label33def.text = "$ \(item.purcahseAmount - item.latestNotifications[0].unitPrice)"
        label33def.textAlignment = .left
        label33def.textColor = UIColor.red
        label33def.font = UIFont(name: "MyriadPro-Regular", size: 15)
        
        let myView = UIView(frame: CGRect(x: 60, y: 65, width: storeView.frame.width/5*4-40, height: 1))
        
        myView.backgroundColor = UIColor.lightGray
        
        
        let alabel1 = UILabel(frame: CGRect(x: 20, y: 70, width: storeView.frame.width/4-10, height: 21))
        alabel1.text = "Date"
        alabel1.font = UIFont(name: "MyriadPro-Regular", size: 10)
        alabel1.textAlignment = .left
        alabel1.textColor = UIColor.gray
        alabel1.font = alabel1.font.withSize(12)
        let a1label2 = UILabel(frame: CGRect(x: storeView.frame.width/4-20, y: 70, width: storeView.frame.width/4+10, height: 21))
        a1label2.text = "Store Details"
        a1label2.textAlignment = .left
        a1label2.textColor = UIColor.gray
        a1label2.font = UIFont(name: "MyriadPro-Regular", size: 10)
        a1label2.font = a1label2.font.withSize(12)
        let a3label3 = UILabel(frame: CGRect(x: (2*storeView.frame.width/4)+30, y: 70, width: storeView.frame.width/4+10, height: 21))
        a3label3.text = "Amount"
        a3label3.textColor = UIColor.gray
        a3label3.textAlignment = .left
        a3label3.font = UIFont(name: "MyriadPro-Regular", size: 10)
        a3label3.font = a3label3.font.withSize(12)
        let a2label3def = UILabel(frame: CGRect(x: (3*storeView.frame.width/4)+10, y: 70, width: storeView.frame.width/4+10, height: 21))
        a2label3def.text = "Difference"
        a2label3def.textColor = UIColor.gray
        a2label3def.textAlignment = .left
        a2label3def.font = UIFont(name: "MyriadPro-Regular", size: 10)
        a2label3def.font = a2label3def.font.withSize(12)
        
        let nlabel11 = UILabel(frame: CGRect(x: 20, y: 75, width: storeView.frame.width/4-10, height: 62))
        //nlabel11.text = item.latestNotifications[1].purchaseDate
        nlabel11.textAlignment = .left
        nlabel11.font = UIFont(name: "MyriadPro-Regular", size: 15)
        nlabel11.textColor = UIColor(red: 7/255, green: 49/255, blue: 118/255, alpha: 1.0)
        let nlabel22 = UILabel(frame: CGRect(x: storeView.frame.width/4-20, y: 75, width: storeView.frame.width/4+10, height: 62))
        //nlabel22.text = item.latestNotifications[1].merchantName
        nlabel22.textAlignment = .left
        nlabel22.font = UIFont(name: "MyriadPro-Regular", size: 15)
        nlabel22.numberOfLines = 0
        nlabel22.textColor = UIColor(red: 7/255, green: 49/255, blue: 118/255, alpha: 1.0)
        let nlabel33 = UILabel(frame: CGRect(x: (2*storeView.frame.width/4)+30, y: 75, width: storeView.frame.width/4-10, height: 62))
        //nlabel33.text = "\(item.latestNotifications[1].unitPrice)"
        nlabel33.textAlignment = .left
        nlabel33.textColor = UIColor(red: 7/255, green: 49/255, blue: 118/255, alpha: 1.0)
        nlabel33.font = UIFont(name: "MyriadPro-Regular", size: 15)
        
        let nlabel33def = UILabel(frame: CGRect(x: (3*storeView.frame.width/4)+10, y: 75, width: storeView.frame.width/4-10, height: 62))
        //nlabel33def.text = "$ \(item.purcahseAmount - item.latestNotifications[2].unitPrice)"
        nlabel33def.textAlignment = .left
        nlabel33def.textColor = UIColor.red
        nlabel33def.font = UIFont(name: "MyriadPro-Regular", size: 15)
        
        let myView1 = UIView(frame: CGRect(x: 60, y: 125, width: storeView.frame.width/5*4-40, height: 1))
        
        myView1.backgroundColor = UIColor.lightGray
        
        let alabel1m = UILabel(frame: CGRect(x: 20, y: 130, width: storeView.frame.width/4-10, height: 21))
        alabel1m.text = "Date"
        alabel1m.font = UIFont(name: "MyriadPro-Regular", size: 15)
        alabel1m.textAlignment = .left
        alabel1m.textColor = UIColor.gray
        alabel1m.font = alabel1m.font.withSize(12)
        let a1label2m = UILabel(frame: CGRect(x: storeView.frame.width/4-20, y: 130, width: storeView.frame.width/4+10, height: 21))
        a1label2m.text = "Store Details"
        a1label2m.textAlignment = .left
        a1label2m.textColor = UIColor.gray
        a1label2m.font = UIFont(name: "MyriadPro-Regular", size: 15)
        a1label2m.font = a1label2m.font.withSize(12)
        let a3label3m = UILabel(frame: CGRect(x: (2*storeView.frame.width/4)+30, y: 130, width: storeView.frame.width/4+10, height: 21))
        a3label3m.text = "Amount"
        a3label3m.textColor = UIColor.gray
        a3label3m.textAlignment = .left
        a3label3m.font = UIFont(name: "MyriadPro-Regular", size: 10)
        a3label3m.font = a3label3m.font.withSize(12)
        let a2label3defm = UILabel(frame: CGRect(x: (3*storeView.frame.width/4)+10, y: 130, width: storeView.frame.width/4+10, height: 21))
        a2label3defm.text = "Difference"
        a2label3defm.textColor = UIColor.gray
        a2label3defm.textAlignment = .left
        a2label3defm.font = UIFont(name: "MyriadPro-Regular", size: 10)
        a2label3defm.font = a2label3defm.font.withSize(12)
        let n1label11 = UILabel(frame: CGRect(x: 20, y: 135, width: storeView.frame.width/4-10, height: 62))
        //n1label11.text = item.latestNotifications[2].purchaseDate
        n1label11.textAlignment = .left
        n1label11.font = UIFont(name: "MyriadPro-Regular", size: 15)
        n1label11.textColor = UIColor(red: 7/255, green: 49/255, blue: 118/255, alpha: 1.0)
        let n1label22 = UILabel(frame: CGRect(x: storeView.frame.width/4-20, y: 135, width: storeView.frame.width/4+10, height: 62))
        //n1label22.text = item.latestNotifications[2].merchantName
        n1label22.textAlignment = .left
        n1label22.font = UIFont(name: "MyriadPro-Regular", size: 15)
        n1label22.numberOfLines = 0
        n1label22.textColor = UIColor(red: 7/255, green: 49/255, blue: 118/255, alpha: 1.0)
        let n1label33 = UILabel(frame: CGRect(x: (2*storeView.frame.width/4)+30, y: 135, width: storeView.frame.width/4-10, height: 62))
        //n1label33.text = "\(item.latestNotifications[2].unitPrice)"
        n1label33.textAlignment = .left
        n1label33.textColor = UIColor(red: 7/255, green: 49/255, blue: 118/255, alpha: 1.0)
        n1label33.font = UIFont(name: "MyriadPro-Regular", size: 15)
        
        let n1label33def = UILabel(frame: CGRect(x: (3*storeView.frame.width/4)+10, y: 135, width: storeView.frame.width/4-10, height: 62))
       // n1label33def.text = "$ \(item.purcahseAmount - item.latestNotifications[2].unitPrice)"
        n1label33def.textAlignment = .left
        n1label33def.textColor = UIColor.red
        n1label33def.font = UIFont(name: "MyriadPro-Regular", size: 15)
        if(item.notificationCount == 0)
        {
            let notlabel = UILabel(frame: CGRect(x: 0, y: storeView.frame.height/2, width: storeView.frame.width, height: 62))
            notlabel.text = "No Notification Available"
            notlabel.textAlignment = .center
            notlabel.textColor = UIColor.black
            notlabel.font = UIFont(name: "MyriadPro-Regular", size: 17
            )
            storeView.addSubview(notlabel)
        }
        
       else if(item.notificationCount == 1)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy"
            let myDate = dateFormatter.date(from: item.latestNotifications[0].purchaseDate)!
            
            dateFormatter.dateFormat = "MM/dd"
            let somedateString = dateFormatter.string(from: myDate)
//            print(somedateString)
            label11.text = somedateString
            label22.text = item.latestNotifications[0].merchantName
            
            label33.text = "$ \(item.latestNotifications[0].newLowerUnitPrice)"
            label33def.text = "$ \(item.latestNotifications[0].redeemAmount)"
            storeView.addSubview(label1)
            storeView.addSubview(label2)
            storeView.addSubview(label3)
            storeView.addSubview(label3def)
            storeView.addSubview(label11)
            storeView.addSubview(label22)
            storeView.addSubview(label33)
            storeView.addSubview(label33def)
            
        }
        else if (item.notificationCount == 2)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy"
            let myDate = dateFormatter.date(from: item.latestNotifications[0].purchaseDate)!
            
            dateFormatter.dateFormat = "MM/dd"
            let somedateString = dateFormatter.string(from: myDate)
            label11.text = somedateString
            label22.text = item.latestNotifications[0].merchantName
            
            label33.text = "$ \(item.latestNotifications[0].newLowerUnitPrice)"
            label33def.text = "$ \(item.latestNotifications[0].redeemAmount)"
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "MM/dd/yyyy"
            let myDate1 = dateFormatter1.date(from: item.latestNotifications[1].purchaseDate)!
            
            dateFormatter1.dateFormat = "MM/dd"
            let somedateString1 = dateFormatter1.string(from: myDate1)
            nlabel11.text = somedateString1
            
            nlabel22.text = item.latestNotifications[1].merchantName
            
            nlabel33.text = "$ \(item.latestNotifications[1].newLowerUnitPrice)"
            nlabel33def.text = "$ \(item.latestNotifications[1].redeemAmount)"
            
            storeView.addSubview(label1)
            storeView.addSubview(label2)
            storeView.addSubview(label3)
            storeView.addSubview(label3def)
            storeView.addSubview(label11)
            storeView.addSubview(label22)
            storeView.addSubview(label33)
            storeView.addSubview(label33def)
            storeView.addSubview(myView)
            storeView.addSubview(alabel1)
            storeView.addSubview(a1label2)
            storeView.addSubview(a3label3)
            storeView.addSubview(a2label3def)
            storeView.addSubview(nlabel11)
            storeView.addSubview(nlabel22)
            storeView.addSubview(nlabel33)
            storeView.addSubview(nlabel33def)
        }
        else if (item.notificationCount == 3){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy"
            let myDate = dateFormatter.date(from: item.latestNotifications[0].purchaseDate)!
            
            dateFormatter.dateFormat = "MM/dd"
            let somedateString = dateFormatter.string(from: myDate)
            label11.text = somedateString
            label22.text = item.latestNotifications[0].merchantName
            
            label33.text = "$ \(item.latestNotifications[0].newLowerUnitPrice)"
            label33def.text = "$ \(item.latestNotifications[0].redeemAmount)"
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "MM/dd/yyyy"
            let myDate1 = dateFormatter1.date(from: item.latestNotifications[1].purchaseDate)!
            
            dateFormatter1.dateFormat = "MM/dd"
            let somedateString1 = dateFormatter1.string(from: myDate1)
            nlabel11.text = somedateString1
            
            nlabel22.text = item.latestNotifications[1].merchantName
            
            nlabel33.text = "$ \(item.latestNotifications[1].newLowerUnitPrice)"
            nlabel33def.text = "$ \(item.latestNotifications[1].redeemAmount)"
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "MM/dd/yyyy"
            let myDate2 = dateFormatter2.date(from: item.latestNotifications[2].purchaseDate)!
            
            dateFormatter2.dateFormat = "MM/dd"
            let somedateString2 = dateFormatter2.string(from: myDate)
            n1label11.text = somedateString2
            
            n1label22.text = item.latestNotifications[2].merchantName
            
            n1label33.text = "$ \(item.latestNotifications[2].newLowerUnitPrice)"
             n1label33def.text = "$ \(item.latestNotifications[2].redeemAmount)"
            storeView.addSubview(label1)
            storeView.addSubview(label2)
            storeView.addSubview(label3)
            storeView.addSubview(label3def)
            storeView.addSubview(label11)
            storeView.addSubview(label22)
            storeView.addSubview(label33)
            storeView.addSubview(label33def)
            storeView.addSubview(myView)
            storeView.addSubview(alabel1)
            storeView.addSubview(a1label2)
            storeView.addSubview(a3label3)
            storeView.addSubview(a2label3def)
            storeView.addSubview(nlabel11)
            storeView.addSubview(nlabel22)
            storeView.addSubview(nlabel33)
            storeView.addSubview(nlabel33def)
            storeView.addSubview(myView1)
            storeView.addSubview(alabel1m)
            storeView.addSubview(a1label2m)
            storeView.addSubview(a3label3m)
            storeView.addSubview(a2label3defm)
            storeView.addSubview(n1label11)
            storeView.addSubview(n1label22)
            storeView.addSubview(n1label33)
            storeView.addSubview(n1label33def)
        }
        
    }
    @IBAction func segmentAct(_ sender: Any) {
//        print(self.item)
        switch segmentview.selectedSegmentIndex {
        case 1:
            for v in storeView.subviews{
                if v is UILabel {
                    v.removeFromSuperview()
                }
                else if v is UIView {
                    v.removeFromSuperview()
                }
            }
            setnotificationview(item: self.item)
            
        case 2:
            for v in storeView.subviews{
                if v is UILabel{
                    v.removeFromSuperview()
                }
                else if v is UIView {
                    v.removeFromSuperview()
                }
                
            }
            updateGraph()
        default:
            for v in storeView.subviews{
                if v is UILabel{
                    v.removeFromSuperview()
                }
                else if v is UIView {
                    v.removeFromSuperview()
                }
            }
            setstoreview(item: self.item)
            
        }
        
    }
    
    func productimageTab() {
//        print("tapped")
        let alertController = UIAlertController(title: "Product", message: "How do you wanna upload ?", preferredStyle: UIAlertControllerStyle.alert)
        
        let openGallery = UIAlertAction(title: "Photos", style: UIAlertActionStyle.default)
        {
            (result : UIAlertAction) -> Void in
            //            print("Open Gallery")
            var selectedDateDictionary = ["selection" : "photos", "productName" : self.productName]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo:selectedDateDictionary)

        }
        let openCamera = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            (result : UIAlertAction) -> Void in
            //            print("Open Camera")
            var selectedDateDictionary = ["selection" : "camera", "productName" : self.productName]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo:selectedDateDictionary)
        }
        alertController.addAction(openGallery)
        alertController.addAction(openCamera)
        self.item.imageExists = false;
        parentViewController?.present(alertController, animated: true, completion: nil)
    }
}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if parentResponder is UIViewController {
                return parentResponder as! UIViewController!
            }
        }
        return nil
    }
}
