//
//  Items.swift
//  lendnow.ai
//
//  Created by Urmila on 11/10/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import Foundation
import UIKit

class Items{
    
    var dayslabel: String
    var detLabel: String
    var amountLabel: String
    var notificationCount: Int
    
    init(dayslabel: String, detLabel: String, amountLabel: String , notificationCount: Int) {
        self.dayslabel = dayslabel
        self.detLabel = detLabel
        self.amountLabel = amountLabel
        self.notificationCount = notificationCount
    }
}
