//
//  Notifications.swift
//  lendnow.ai
//
//  Created by Sundar Gsv on 11/11/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import Foundation
import ObjectMapper

class Notifications:  NSObject, Mappable
{
    var id: String = ""
    var merchantName: String = ""
    var merchantAddress: String = ""
    var merchantPhoneNumber: String = ""
    var productName: String = ""
    var unitPrice: Double = 0.00
    var purchaseDate: String = ""
    var newLowerUnitPrice: Double = 0.00
    var notificationDate: String = ""
    var expiringDays: String = ""
    var redeemAmount: Double = 0.00
    
    
    override init() {
        super.init()
    }
    
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    /*
     * The below struct is used to represent the model without any problem using Alamofire
     * Eventhough the keys were changed from Back-end. Just change the struct accordingly which
     * prevents changing the all over the code
     */
    
    func mapping(map: Map) {
        
        id <- map["id"]
        merchantName <- map["merchantName"]
        merchantAddress <- map["merchantAddress"]
        merchantPhoneNumber <- map["merchantPhoneNumber"]
        productName <- map["productName"]
        unitPrice <- map["unitPrice"]
        purchaseDate <- map["purchaseDate"]
        newLowerUnitPrice <- map["newLowerUnitPrice"]
        notificationDate <- map["notificationDate"]
        expiringDays <- map["expiringDays"]
        redeemAmount <- map["redeemAmount"]
    }
}
