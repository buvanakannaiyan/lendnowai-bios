//
//  PurchaseService.swift
//  lendnow.ai
//
//  Created by Sundar Gsv on 19/09/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
class PurchaseService {
    
    let consumerAIBaseURL: URL!
    var productNameQueryStringKey: String = "productNameFilter="
    
    init()
    {
        consumerAIBaseURL = URL(string: "http://consumerai.centralus.cloudapp.azure.com:8080/consumerai_core/")
    }
    
    func getPurchaseByDays(userPhoneNumber: String, noOfDays: String, searchQuery : String, completion: @escaping (_ result: [Purchase]) -> Void)
        
    {
        
        
        var purchaseArray = [AnyObject]()
        
        // Get request
        let purchaseBaseURL: String = "purchaseRecords?userPhoneNumber="
        
        if var purchaseURL = URL(string: "\(consumerAIBaseURL!)\(purchaseBaseURL)\(userPhoneNumber)") {
            
            if noOfDays == "30" {
                purchaseURL = URL(string: "\(purchaseURL)&noOfPastDay=\(noOfDays)")!
            }
            if searchQuery != "" {
                purchaseURL = URL(string: "\(purchaseURL)&productNameFilter=\(searchQuery)")!
            }

        
            Alamofire.request(purchaseURL).responseArray { (response: DataResponse<[Purchase]>) in

                let purchaseArray = response.result.value
                print(purchaseArray?.count ?? 0)
                    completion(purchaseArray!)
                }
            }
        
        }
    
    func getStaticPurchase(userPhoneNumber: String, searchQuery : String, pageIndex : Int, completion: @escaping (_ result: [Purchase]) -> Void)
        
    {
        
        
        print("inside purchasse service")
        print("pageIndex \(pageIndex)")
        
        var purchaseURL: String = "http://consumerai.centralus.cloudapp.azure.com:8080/consumerai_core/transactions?userPhoneNumber=\(userPhoneNumber)"
      print("==============================")
        print(purchaseURL)
        Alamofire.request(purchaseURL).responseArray(keyPath: "results") { (response: DataResponse<[Purchase]>) in
            
            let transactions = response.result.value
           
            completion(transactions!)
        }
        
    }
    
    func getNotificationByProductName(userPhoneNumber: String, productName : String, completion: @escaping (_ result: [Notifications]) -> Void)
        
    {
        
        let notificationURL = "http://api.bozucu.ai/consumerai_core/notificationRecords?userPhoneNumber=\(userPhoneNumber)&productNameFilter="
        print(notificationURL ?? "EMPTY")
        Alamofire.request(notificationURL).responseArray { (response: DataResponse<[Notifications]>) in
            
            let notifications = response.result.value
            
            completion(notifications!)
        }
        
    }
        
        
}
    

