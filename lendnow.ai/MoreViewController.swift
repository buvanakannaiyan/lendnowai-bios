//
//  MoreViewController.swift
//  lendnow.ai
//
//  Created by Nishant Kumar on 15/10/17.
//  Copyright © 2017 Magnum Opus, Inc. All rights reserved.
//

import UIKit



class MoreViewController: UIViewController {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
